#include "NameErrorException.h"

NameErrorException::NameErrorException(string name)
{
	this->_error_message = string("NameError: name '" + name + "' is not defined");
}

const char * NameErrorException::what() const throw()
{
	return this->_error_message.c_str();
}
