#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H

#include "InterperterException.h"
#include <string>

using std::string;

class NameErrorException : public InterperterException
{
public:
	NameErrorException(string name);
	virtual const char* what() const throw();

private:
	string _error_message;
};

#endif // NAME_ERROR_EXCEPTION_H