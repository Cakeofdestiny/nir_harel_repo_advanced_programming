#ifndef TYPE_H
#define TYPE_H

#include <string>

using std::string;

class Type
{
public:
	Type();
	Type(bool isTemp);
	virtual ~Type();

	bool getIsTemp() const;
	void setIsTemp(bool isTemp);

	virtual bool isPrintable() const = 0;
	virtual string toString() const = 0;
	//Return a pointer to a new type - NOTE - DYNAMIC ALLOCATION
	virtual Type* copy() = 0;

private:
	bool _isTemp;
};


#endif //TYPE_H
