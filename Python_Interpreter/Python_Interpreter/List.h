#ifndef LIST_H
#define LIST_H

#include "Sequence.h"
#define LIST_DELIM ","
#define SEP ", "
#define START_CHAR "["
#define END_CHAR "]"

class List : public Sequence
{
public:
	List(std::vector<Type*> varList);
	List(std::vector<Type*> varList, bool isTemp);
	~List();


	virtual bool isPrintable() const;
	virtual string toString() const;
	virtual Type* copy();

private:
	std::vector<Type*> _varList;

};


#endif // LIST_H