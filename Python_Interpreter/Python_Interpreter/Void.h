#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
public:
	Void();
	Void(bool isTemp);

	virtual bool isPrintable() const;
	virtual string toString() const;
	virtual Type* copy();

private:

};


#endif // VOID_H