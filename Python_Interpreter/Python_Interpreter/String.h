#ifndef STRING_H
#define STRING_H

#include "type.h"

class String : public Type
{
public:
	String(string value);
	String(string value, bool isTemp);

	virtual bool isPrintable() const;
	virtual string toString() const;
	virtual Type* copy();

private:
	string _value;
};


#endif // STRING_H