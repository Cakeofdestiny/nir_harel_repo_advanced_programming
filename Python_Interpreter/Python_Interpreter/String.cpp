#include "String.h"

String::String(string value)
{
	this->_value = value;
}

String::String(string value, bool isTemp) : Type(isTemp)
{
	this->_value = value;
}

bool String::isPrintable() const
{
	return true;
}

string String::toString() const
{
	return this->_value;
}

Type * String::copy() 
{
	return new String(this->_value, this->getIsTemp());
}


