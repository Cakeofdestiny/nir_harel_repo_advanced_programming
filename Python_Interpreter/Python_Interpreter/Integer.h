#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
public:
	Integer(int value);
	Integer(int value, bool isTemp);

	virtual bool isPrintable() const;
	virtual string toString() const;
	virtual Type* copy();

private:
	int _value;
};


#endif // INTEGER_H