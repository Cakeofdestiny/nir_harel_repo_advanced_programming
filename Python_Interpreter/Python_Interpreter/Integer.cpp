#include "Integer.h"

Integer::Integer(int value)
{
	this->_value = value;
}

Integer::Integer(int value, bool isTemp) : Type(isTemp)
{
	this->_value = value;
}

bool Integer::isPrintable() const
{
	return true;
}

string Integer::toString() const
{
	return std::to_string(this->_value);
}

Type * Integer::copy() 
{
	return new Integer(this->_value, this->getIsTemp());
}
