#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Nir Harel"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;
	Type* var = nullptr;
	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()" && input_string != "exit()")
	{
		var = nullptr;
		// parsing command
		try
		{
			var = Parser::parseString(input_string);
		}
		catch (InterperterException& exc)
		{
			std::cout << exc.what() << std::endl;
		}
		if (var != nullptr)
		{
			if (var->isPrintable())
			{
				std::cout << var->toString() << std::endl;
			}

			//Free the variable's memory if it's temporary
			if (var->getIsTemp())
			{
				delete var;
				//reset var
				var = nullptr;
			}
		}	

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	Parser::clearMemory();

	return 0;
}


