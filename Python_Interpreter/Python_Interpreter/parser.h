#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "type.h"
#include "List.h"
#include "String.h"
#include "Void.h"
#include "Integer.h"
#include "Boolean.h"

#include "Helper.h"

#include <string>
#include <unordered_map>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <regex>

#define VAR_NAME_PATTERN "^[a-zA-Z_][a-zA-Z0-9_]*$"
#define TAB '\t'
#define SPACE ' '
#define ASSIGNMENT "="

constexpr char SINGLE_QUOTE = '\'';

class Parser
{
public:
	static Type* parseString(std::string str);
	static Type* getType(std::string& str, bool temp);
	static void clearMemory();

private:
	static bool _runChecks(std::string& str);
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string& str);
	static std::unordered_map<string, Type*> _variables;
	static Type* parseList(std::string str);
	
};

#endif //PARSER_H
