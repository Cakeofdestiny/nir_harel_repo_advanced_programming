#include "parser.h"
#include <iostream>

std::unordered_map<string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str)
{
	Helper::rtrim(str);
	Type* var = nullptr;
	bool assignment_res = false;

	if (Parser::_runChecks(str))
	{
		//If it's legal, try to acquire a variable. No need to try to acquire a var if it isn't legal.
		if (Parser::isLegalVarName(str))
		{
			var = Parser::getVariableValue(str);
		}

		if (var == nullptr)
		{
			var = Parser::getType(str, true);
			//If it isn't just a variable
			if (var == nullptr)
			{
				assignment_res = Parser::makeAssignment(str);
				if (!assignment_res)
				{
					throw SyntaxException();
				}
				//Otherwise, return a temporary void
				var = new Void(true);
			}
		}
		
	}
	
	return var;
}

Type* Parser::getType(std::string& str, bool temp)
{
	Type* var = nullptr;
	//Remove leading and trailing spaces
	Helper::trim(str);

	if (str.length() != 0)
	{
		if (Helper::isString(str))
		{
			str[0] = SINGLE_QUOTE;
			str.back() = SINGLE_QUOTE;
			var = new String(str, temp);
		}
		else if (str == TRUE)
		{
			var = new Boolean(true, temp);
		}
		else if (str == FALSE)
		{
			var = new Boolean(false, temp);
		}
		else if (Helper::isInteger(str))
		{
			var = new Integer(std::stoi(str), temp);
		}
		else if (Helper::isList(str))
		{
			//A separate func because it's a bit long
			var = Parser::parseList(str);
		}
	}
	
	return var;
}

void Parser::clearMemory()
{
	//It's alright to just delete the type*, as it's the only dynamic thing
	for (std::unordered_map<string, Type*>::iterator it = Parser::_variables.begin();
		it != Parser::_variables.end();
		++it)
	{
		//Free memory and set it to null to make sure nobody tries to print it
		delete it->second;
		it->second = nullptr;
	}
}

bool Parser::_runChecks(std::string& str)
{
	bool success = true;
	if (str.length() == 0)
	{
		success = false;
	}
	else if (str[0] == TAB || str[0] == SPACE)
	{
		success = false;
		throw IndentationException();
	}

	return success;
}

bool Parser::isLegalVarName(const std::string & str)
{
	return std::regex_match(str, std::regex(VAR_NAME_PATTERN));
}

bool Parser::makeAssignment(const std::string & str)
{
	bool assigned = true;
	if (str.find(ASSIGNMENT) == std::string::npos)
	{
		assigned = false;
	}
	//If it contained an equal sign
	if (assigned)
	{
		//Parse into name and value
		string name = str.substr(0, str.find(ASSIGNMENT));
		string value = str.substr(str.find(ASSIGNMENT) + 1);
		Type* var = nullptr;

		//Trim whitespace
		Helper::trim(value);
		Helper::rtrim(name);

		if (!isLegalVarName(name))
		{
			throw SyntaxException();
		}
		
		//It's important to separate these to two  ifs to avoid leaks
		var = getType(value, false);
		if (var == nullptr)
		{
			//If it's not a direct type, try to search for the value
			if (Parser::isLegalVarName(value))
			{
				var = Parser::getVariableValue(value);
				if (var != nullptr)
				{
					//Copy the var
					var = var->copy();
				}
				else
				{
					//Otherwise, throw an exception because it's neither a copy nor a variable name
					throw SyntaxException();
				}
			}
			else
			{
				throw SyntaxException();
			}
		}
		//Insert the variable into the dictionary
		Parser::_variables[name] = var;
	}
	
	return assigned;
}

Type* Parser::getVariableValue(const std::string & str)
{
	Type* var = nullptr;
	//Store the iterator because it's faster
	std::unordered_map<string, Type*>::iterator it = Parser::_variables.find(str);
	if (it != Parser::_variables.end())
	{
		var = it->second;
	}
	else
	{
		throw NameErrorException(str);
	}

	return var;
}

Type * Parser::parseList(std::string str)
{
	//Trim the str from the list markers
	str = str.substr(1, str.length()-2);
	unsigned int start = 0U;
	unsigned int end = str.find(LIST_DELIM);
	Type* var = nullptr;
	std::vector<Type*> items;
	string current_value = "";
	bool over = false;

	while (!over)
	{
		current_value = str.substr(start, end - start);
		over = end == string::npos;
		start = end + 1;
		end = str.find(LIST_DELIM, start);
		//Set the new end as the end, and increment start to one after it

		var = Parser::getType(current_value, false);
		//If it failed
		if (var == nullptr)
		{
			for (int i = 0; i < items.size(); i++)
			{
				//Delete the items that we acquired so far
				delete items[i];
			}
			//And throw a syntax exception
			throw SyntaxException();
		}
		else
		{
			items.push_back(var);
		}
	} 

	return new List(items);
}


