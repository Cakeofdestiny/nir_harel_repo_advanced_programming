#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"
#include <vector>

class Sequence : public Type
{
public:
	Sequence();
	Sequence(bool isTemp);

};

#endif // SEQUENCE_H