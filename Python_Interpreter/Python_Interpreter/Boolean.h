#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"

#define TRUE "True"
#define FALSE "False"

class Boolean : public Type
{
public:
	Boolean(bool value);
	Boolean(bool value, bool isTemp);

	virtual bool isPrintable() const;
	virtual string toString() const;
	virtual Type* copy();

private:
	bool _value;
};

#endif // BOOLEAN_H