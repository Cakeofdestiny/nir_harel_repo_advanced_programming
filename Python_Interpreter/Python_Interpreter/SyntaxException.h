#ifndef SYNTAX_EXCEPTION_H
#define SYNTAX_EXCEPTION_H

#include "InterperterException.h"
#define SYNTAX_EXCEPTION_MESSAGE "SyntaxError: invalid syntax"

class SyntaxException : public InterperterException
{
public:
	virtual const char* what() const throw();
}; 


#endif // SYNTAX_EXCEPTION_H