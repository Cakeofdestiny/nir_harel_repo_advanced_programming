#include "Boolean.h"

Boolean::Boolean(bool value)
{
	this->_value = value;
}

Boolean::Boolean(bool value, bool isTemp) : Type(isTemp)
{
	this->_value = value;
}

bool Boolean::isPrintable() const
{
	return true;
}

string Boolean::toString() const
{
	return this->_value ? TRUE : FALSE;
}

Type * Boolean::copy() 
{
	return new Boolean(this->_value, this->getIsTemp());
}
