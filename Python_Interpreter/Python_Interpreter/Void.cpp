#include "Void.h"

Void::Void()
{
}

Void::Void(bool isTemp) : Type(isTemp)
{
}

bool Void::isPrintable() const
{
	return false;
}

string Void::toString() const
{
	return "";
}

Type * Void::copy() 
{
	return new Void(this->getIsTemp());
}
