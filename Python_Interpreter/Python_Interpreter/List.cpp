#include "List.h"

List::List(std::vector<Type*> varList) : Sequence()
{
	this->_varList = varList;
}

List::List(std::vector<Type*> varList, bool isTemp) : Sequence(isTemp)
{
	this->_varList = varList;
}

List::~List()
{
	for (std::vector<Type*>::iterator it = this->_varList.begin();
		it != this->_varList.end();
		++it)
	{
		delete *it;
	}
}

bool List::isPrintable() const
{
	return true;
}

string List::toString() const
{
	string rep = START_CHAR;
	
	for (size_t i = 0; i < this->_varList.size(); i++)
	{
		rep += this->_varList[i]->toString();
		if (i+1 != this->_varList.size())
		{
			rep += SEP;
		}
	}

	rep += END_CHAR;
	return rep;
}

Type* List::copy()
{
	return this;
}
