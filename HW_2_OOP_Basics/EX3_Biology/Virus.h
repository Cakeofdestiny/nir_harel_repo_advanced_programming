#pragma once
#include <string>
#include <algorithm>
#include "Cell.h"

using std::string;

class Virus
{
public:
	void init(const string RNA_sequence);
	void infect_cell(Cell& cell) const;

private:
	string _RNA_sequence;
};