#pragma once
#include "Nucleus.h"
#include "Mitochondrion.h"
#include "Ribosome.h"
#define ATP_SUCCESS_UNITS 100

using std::string;

class Cell
{

public:
	void init(const string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
	//getter
	Nucleus& get_nucleus_reference(); //We have to make this a non const since we're returning a reference.

private: //private variables of organelles
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glucose_receptor_gene;
	unsigned int _atp_units;

};
