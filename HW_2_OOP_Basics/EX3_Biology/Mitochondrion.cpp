#include "Mitochondrion.h"

//Init mitochondrion
void Mitochondrion::init()
{
	this->_glucose_level = 0;
	this->_has_glucose_receptor = false;
}

//Receives a protein chain, and updates _has_glucose_receptor to true if it's of a configuration matching it.
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* currNode = protein.get_first();
	bool equal_chains = true;

	int i = 0;

	//iterate until we reach the end of the linked list or the end of the sequence.
	for (i=0; i<GLUCOSE_RECEPTOR_SIZE && currNode != nullptr; i++)
	{
		equal_chains = currNode->get_data() == Mitochondrion::glucose_receptor[i]; //Check if they're equal
		currNode = currNode->get_next(); //go the next node
	}

	if (equal_chains && i==GLUCOSE_RECEPTOR_SIZE && currNode == nullptr) //Check if we passed over the entirety of both chains, and they were equal.
	{
		this->_has_glucose_receptor = true; //if it is, set to true
		this->_glucose_level = GLUCOSE_AMOUNT; // set the glucose level to 50
	}

}

/* I: glucose level
 * Changes the glucose level for the Mitochondrion*/
void Mitochondrion::set_glucose(const unsigned int glucose_units)
{
	this->_glucose_level = glucose_units;
}

//Return true if we have a glucose receptor, and over 50 glucose available.
bool Mitochondrion::produceATP() const
{
	return this->_has_glucose_receptor && this->_glucose_level >= 50;
}
