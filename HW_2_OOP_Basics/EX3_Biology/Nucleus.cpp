#include "Nucleus.h"

//Initialize the gene
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//GETTERS

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

//SETTERS

void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}

void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

//NUCLEUS Class

//Init nucleus with a dna string
void Nucleus::init(const string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = string(this->_DNA_strand.size(), ' '); //create an empty string with an appropriate size
	//Transform the DNA strand into its complementary one, using _complement_char as the conversion function.
	std::transform(this->_DNA_strand.begin(), this->_DNA_strand.end(), this->_complementary_DNA_strand.begin(), _complement_char);
}


//DNA Strand getter
string Nucleus::get_DNA_strand() const
{
	return this->_DNA_strand;
}


//Return the RNA transcript of the gene with the details defined by the Gene instance ref
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string source_dna = ""; //Initially, I wanted to do this using references (instead of copying the string), but I know that you guys don't like it when variables are not defined at the beginning of a scope.
	string output_rna(gene.get_end() - gene.get_start() + 1, ' '); //properly create an empty string for the iterator
	//Get proper string
	if (gene.is_on_complementary_dna_strand())
	{
		source_dna = this->_complementary_DNA_strand;
	}else
	{
		source_dna = this->_DNA_strand;
	}

	/*Use transform to cut the dna strand and transform to RNA.
	/Send the proper iterators with their offsets, and output to the rna string.
	/We need to use +1 since we want up until that char, inclusive.*/
	std::transform(source_dna.begin() + gene.get_start(), source_dna.begin() + gene.get_end() + 1, output_rna.begin(), RNA_char);

	return output_rna;
}

//Reverse a string.
string Nucleus::get_reversed_DNA_strand() const
{
	string resp = this->_DNA_strand; //create it
	std::reverse(resp.begin(), resp.end()); //reverse it
	return resp;
}

//Get the number of codon (a three letter sequence) appearences in the dna strand.
unsigned int Nucleus::get_num_of_codon_appearences(const string& codon) const
{
	int count = 0;
	unsigned int i = 0;
	unsigned int found_loc = 0;

	
	for (i=0; i<this->_DNA_strand.size(); i++) //Go over the string
	{
		found_loc = this->_DNA_strand.find(codon, i);
		if (found_loc != string::npos)
		{
			i = found_loc + 1; //set I to the next occurence
			count++;
		}
	}
	return count;
}


/*Static methods
//I: a char
//O: its complement. 
//A simple function to complement a char. Quits forcefully if the letter is invalid.*/
char Nucleus::_complement_char(const char c)
{
	char resp = 0;
	switch (c)
	{
	case 'a':
	case 'A':
		resp = 'T';
		break;

	case 'c':
	case 'C':
		resp = 'G';
		break;

	case 't':
	case 'T':
		resp = 'A';
		break;

	case 'g':
	case 'G':
		resp = 'C';
		break;

	default:
		std::cerr << "Invalid DNA letter. There is no complement for " << c;
		_exit(1);
	}

	return resp;
}


//Convert a char to and from RNA.
char Nucleus::RNA_char(const char c)
{
	char resp = 0;
	switch (c)
	{
	case 'T':
	case 't':
		resp = 'U';
		break;

	case 'u':
	case 'U':
		resp = 'T';
		break;
	default: //we don't need to deal with invalid letters here, init dealt with them.
		resp = c;
	}
	return resp;
}

