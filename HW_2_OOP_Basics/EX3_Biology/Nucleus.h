#pragma once
#include <string>
#include <iostream>
#include <algorithm>

using std::string;

class Gene
{
public:
	//init method
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	//getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	//setters
	void set_start(const unsigned int start);
	void set_end(const unsigned int end);
	void set_complementary_dna_strand(const bool on_complementary_dna_strand);

private: //vars
	unsigned int _start; //gene start index
	unsigned int _end;   //gene end index
	bool _on_complementary_dna_strand; //if it's on the complementary strand

};

class Nucleus
{
public:
	//init func
	void init(const string dna_sequence);

	//getters
	string get_DNA_strand() const;


	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearences(const string& codon) const;

	static char RNA_char(const char c);
	
private:
	string _DNA_strand;
	string _complementary_DNA_strand;
	//Converts a letter to its complement
	static char _complement_char(const char c);
	
};


