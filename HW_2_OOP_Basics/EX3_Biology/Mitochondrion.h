#pragma once
#include "Protein.h"
#include "AminoAcid.h"

#define GLUCOSE_RECEPTOR_SIZE 7
#define GLUCOSE_AMOUNT 50

class Mitochondrion
{
public:
	void init();

	//sets  _has_glucose_receptor to true if the chain matches
	void insert_glucose_receptor(const Protein & protein);

	//Setter
	void set_glucose(const unsigned int glucose_units);

	bool produceATP() const;

private:
	unsigned int _glucose_level;
	bool _has_glucose_receptor;
	static constexpr AminoAcid glucose_receptor[GLUCOSE_RECEPTOR_SIZE] = {ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END}; //A static const list to check for glucose validity 

};
