#pragma once
#include "Protein.h"
#define CODON_LENGTH 3 

using std::string;

class Ribosome
{
public:
	Protein* create_protein(string &RNA_transcript) const;
};
