#include "Cell.h"

//Inits the nucleus.
void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glucose_receptor_gene = glucose_receptor_gene;
}

//Tries to produce ATP using its organelles
bool Cell::get_ATP()
{
	bool success = true;
	string receptor_transcript = this->_nucleus.get_RNA_transcript(this->_glucose_receptor_gene); //get the rna transcript for our receptor gene
	Protein* receptor_protein = this->_ribosome.create_protein(receptor_transcript); //create the protein
	
	if (receptor_protein == nullptr) //if we didn't manage to create the protein
	{
		std::cerr << "I didn't manage to create a receptor protein.";
		_exit(EXIT_FAILURE);
		
	}

	this->_mitochondrion.insert_glucose_receptor(*receptor_protein); //send the receptor protein's value to the mitochondrion
	success = this->_mitochondrion.produceATP(); //check if we managed to produce atp

	if (success) //if it can produce atp
	{
		this->_atp_units = ATP_SUCCESS_UNITS; //set to 100
	}

	delete receptor_protein; //free the memory allocated by create_protein
	return success; 
}

Nucleus & Cell::get_nucleus_reference()  //this can't be const, because we're returning a reference.
{
	return this->_nucleus;
}
