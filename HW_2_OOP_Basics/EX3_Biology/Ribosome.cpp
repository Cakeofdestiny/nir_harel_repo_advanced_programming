#include "Ribosome.h"

Protein * Ribosome::create_protein(string & RNA_transcript) const
{
	Protein* protein = new Protein(); //dynamically get a new protein
	AminoAcid acid = UNKNOWN; //amino acid enum
	string codon; //three letter codon
	bool validFlag = true; //flag to signify if we found an invalid amino acid
	protein->init();
	//while we have have more than 3 letters in our rna transcript
	while (RNA_transcript.size() >= CODON_LENGTH && validFlag)
	{
		codon = RNA_transcript.substr(0, CODON_LENGTH); //get the first three letters
		RNA_transcript = RNA_transcript.substr(CODON_LENGTH, RNA_transcript.size()); //concat to the rest
		acid = get_amino_acid(codon); //get the amino acid for this codon

		if (acid == UNKNOWN) //if the acid is unknown
		{
			validFlag = false; //set to invalid
			protein->clear(); //clear the lists content
			delete protein;   //free the protein's class memory
			protein = nullptr;  //set to nullptr, as per the instructions.
		}else
		{
			protein->add(acid);  //if it's valid add it to the protein
		}
	}

	return protein;
}
