#include "Virus.h"

void Virus::init(const string RNA_sequence)
{
	this->_RNA_sequence = RNA_sequence;
}

void Virus::infect_cell(Cell & cell) const
{
	Nucleus& nucleus = cell.get_nucleus_reference();
	string cell_dna = nucleus.get_DNA_strand(); //get the nucleus dna from the cell
	string virus_dna(this->_RNA_sequence.size(), 0); //we need to convert rna to dna
	unsigned int middle_index = cell_dna.size() / 2; //calculate the middle of the cell dna

	std::transform(this->_RNA_sequence.begin(), this->_RNA_sequence.end(), virus_dna.begin(), Nucleus::RNA_char); //Transform from RNA to dna using the rna transformer
	cell_dna = cell_dna.substr(0, middle_index) + virus_dna + cell_dna.substr(middle_index); //Create substrings halfway through and insert our dna in the middle

	nucleus.init(cell_dna); //change its cell dna
}
