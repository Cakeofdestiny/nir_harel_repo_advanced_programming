1.1. In C++ references allow us to create a second reference to the memory location of an existing variable.
We can define a reference with an ampersand.
For example, to create a reference to "int x" we can use:
int& x_ref = x;

1.2. First, when passing a reference to a variable we don't need to copy it. This saves both time and memory. It's less apparent with small
primitive variables, but it can be much more significant with large classes. Additionally, the reference can simply be used as a regular variable in
the function, making the code cleaner.

1.3. References are safer because they have to be initialized. This means that unless the programmer
specifically requests it (with rather bizarre code), the reference will not cause a segfault. This is, of course,
as long as the reference's scope isn't freed. A pointer can essentially point to any memory address, and while it gives
you more control, it's prone to more errors.

1.4.a. square(3,y) - Valid.
1.4.b. square(3,&y) - Invalid. We're sending an int* to an int& reference that expects to receive a plain int.
Besides, references must be initialized with an already existing variable.
1.4.c. square(3,6) - Invalid. A reference must be initialized with an existing variable, and cannot be initialized with an immediate.
An immediate is stored as a part of the instruction, and not as a variable in memory.

1.5.a. The function creates an int variable and returns a reference to it. On the surface, this looks fine, however, this int
will no longer exist as soon as the function exits, and using the reference will likely cause a segfault.

1.5.b. While this function is technically correct, it doesn't follow very good practices. Assigning refrences to dynamic memory is a bad idea,
as their whole point is that the memory is managed somewhere else, probably in the stack.