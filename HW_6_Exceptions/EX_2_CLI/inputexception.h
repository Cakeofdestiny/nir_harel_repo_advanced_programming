#pragma once
#include <exception>

class inputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This is an input exception! You probably tried to enter a char where a number was needed.";
	}
};