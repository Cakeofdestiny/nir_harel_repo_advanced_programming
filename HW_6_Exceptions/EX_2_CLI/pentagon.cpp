#include "pentagon.h"

Pentagon::Pentagon(std::string name, std::string color, double side) : Shape(color, name)
{
	this->setSide(side);
}

void Pentagon::draw()
{
	std::cout << "Shape: Pentagon" << std::endl
		<< "Color: " << this->getColor() << std::endl
		<< "Name: " << this->getName() << std::endl
		<< "Side: " << this->_side << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double Pentagon::CalArea()
{
	return MathUtils::calPentagonArea(this->_side);
}

void Pentagon::setSide(double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

double Pentagon::getSide()
{

	return this->_side;
}
