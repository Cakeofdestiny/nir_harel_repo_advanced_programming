#include "hexagon.h"

Hexagon::Hexagon(std::string name, std::string color, double side) : Shape(color, name)
{
	this->setSide(side);
}

void Hexagon::draw()
{
	std::cout << "Shape: Hexagon" << std::endl
		<< "Color: " << this->getColor() << std::endl
		<< "Name: " << this->getName() << std::endl
		<< "Side: " << this->_side << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double Hexagon::CalArea()
{
	return MathUtils::calHexagonArea(this->_side);
}

void Hexagon::setSide(double side)
{
	if (side < 0) //throw a shape exception if the side is illegal
	{
		throw shapeException();
	}
	this->_side = side;
}

double Hexagon::getSide()
{
	return this->_side;
}
