#pragma once

#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Pentagon : public Shape
{
public:
	Pentagon(std::string name, std::string color, double side);

	void draw();
	double CalArea();
	
	void setSide(double side);
	double getSide();
private:
	double _side;

};
