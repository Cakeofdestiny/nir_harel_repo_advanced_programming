#include "MathUtils.h"

double MathUtils::calPentagonArea(double length)
{
	//Formula: AREA_CONSTANT * SIDE^2
	return PENTAGON_AREA_CONSTANT * pow(length, 2);
}

double MathUtils::calHexagonArea(double length)
{
	//Formula: AREA_CONSTANT * SIDE^2
	return HEXAGON_AREA_CONSTANT * pow(length, 2);
}
