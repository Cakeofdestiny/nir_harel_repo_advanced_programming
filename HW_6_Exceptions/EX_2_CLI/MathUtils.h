#pragma once
#include <cmath>

#define PENTAGON_AREA_CONSTANT 1.720477401
#define HEXAGON_AREA_CONSTANT 2.598076211

class MathUtils
{
public:
	static double calPentagonArea(double length); //a function to calculate the area of a pentagon
	static double calHexagonArea(double length); //a function to calculate the area of a hexagon

};
