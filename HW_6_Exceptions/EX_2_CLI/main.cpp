#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "handlingCin.h"
#include "inputexception.h"
#include "hexagon.h"
#include "pentagon.h"

#define CIRCLE 'c'
#define QUADRILATERAL 'q'
#define RECTANGLE 'r'
#define PARALLELOGRAM 'p'
#define HEXAGON 'h'
#define PENTAGON 't'
#define EXIT_CHAR 'x'

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);

	Hexagon hexa(nam, col, side);
	Pentagon penta(nam, col, side); //just add these like the rest

	std::string in;

	handlingCin hcin;

	std::cout << "Enter information for your objects" << std::endl;
	char shapetype;
	char userExit = 0;
	while (userExit != EXIT_CHAR) {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, t = pentagon" << std::endl;
		hcin >> shapetype;

		getline(std::cin, in); //get a line into a string to determine if the user entered more than a single char as requested.
		if (in.length() > 0)
		{
			std::cout << "Warning - Don't try to build more than one shape at once." << std::endl;
		}

		try
		{
			switch (shapetype) {
			case CIRCLE:
				std::cout << "enter color, name,  rad for circle" << std::endl;
				hcin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				circ.draw();
				break;
			case QUADRILATERAL:
				std::cout << "enter name, color, height, width" << std::endl;
				hcin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				quad.draw();
				break;
			case RECTANGLE:
				std::cout << "enter name, color, height, width" << std::endl;
				hcin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				rec.draw();
				break;
			case PARALLELOGRAM:
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				hcin >> nam >> col >> height >> width >> ang >> ang2;

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				para.draw();
				break;

			case PENTAGON:
				std::cout << "Enter name, color, and side for the pentagon." << std::endl;
				hcin >> nam >> col >> side;

				penta.setName(nam);
				penta.setColor(col);
				penta.setSide(side);
				penta.draw();
				break;

			case HEXAGON:
				std::cout << "Enter name, color, and side for the hexagon." << std::endl;
				hcin >> nam >> col >> side;

				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setSide(side);
				hexa.draw();
				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press " << EXIT_CHAR << std::endl;
			hcin >> userExit;
			cin.ignore(INT_MAX, '\n'); //we should clear it here too if the user entered "cpf" here for example

		}
		catch (inputException &inpExp)
		{
			printf(inpExp.what());
			std::cout << std::endl;
			cin.clear();
			cin.ignore(INT_MAX, '\n'); //Clear and flush cin because the user entered wrong values
		}
		catch (std::exception &e) //exception was not sent as reference before, so it was sliced
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;
}