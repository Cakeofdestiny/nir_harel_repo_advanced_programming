#pragma once

#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string name, std::string color, double side);

	void draw();
	double CalArea();

	void setSide(double side);
	double getSide();
private:
	double _side;

};
#pragma once
