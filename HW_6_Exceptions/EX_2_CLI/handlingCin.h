#pragma once

#include <iostream>

#include "inputexception.h"

using std::cin;

class handlingCin
{
public:
	handlingCin();
	~handlingCin();

	template <class T>
	handlingCin& operator>>(T& t);
};

template<class T> //This must be here and not in the cpp file because of some template stuff. Could've used inline but eh
handlingCin & handlingCin::operator>>(T & t)
{
	cin >> t;
	if (cin.fail())
	{
		throw(inputException());
	}
	return *this;
}
