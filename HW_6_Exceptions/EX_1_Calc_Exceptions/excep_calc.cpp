#include <iostream>
#define ILLEGAL_NUM 8200
#define ERR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year."

int add(int a, int b) {
	int sum = a + b;
	if (sum == ILLEGAL_NUM)
	{
		throw(ERR_MSG);
	}

  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
	if (sum == ILLEGAL_NUM || i == ILLEGAL_NUM)
	{
		throw(ERR_MSG);
	}
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
	if (exponent == ILLEGAL_NUM || i == ILLEGAL_NUM)
	{
		throw(ERR_MSG);
	}
  };
  return exponent;
}

int main(void) {
	try
	{
		std::cout << pow(8200, 1) << std::endl; //invalid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

	try
	{
		std::cout << pow(5, 5) << std::endl; //valid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

	try
	{
		std::cout << multiply(2050, 5) << std::endl; //invalid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

	try
	{
		std::cout << multiply(2050, 3) << std::endl; //valid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

	try
	{
		std::cout << add(2050, 6150) << std::endl; //invalid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}

	try
	{
		std::cout << add(2050, 3) << std::endl; //valid
	}
	catch (const char* msg)
	{
		std::cout << msg << std::endl;
	}
	
	getchar();
}