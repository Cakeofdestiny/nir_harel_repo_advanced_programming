#include <iostream>

#define ERR_CODE 82008200
#define ILLEGAL_NUM 8200
#define ERR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearence in 1 year."

int add(int a, int b, int& err_code) {
	int sum = a + b;
	
	err_code = 0;
	if (sum == ILLEGAL_NUM)
	{
		err_code = ERR_CODE;
	}

  return sum;
}

int  multiply(int a, int b, int& err_code) {
  int sum = 0;
  int temp_code = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, temp_code);
	if (sum == ILLEGAL_NUM || i == ILLEGAL_NUM || temp_code == ERR_CODE) //make sure that even i isn't illegal
	{
		err_code = ERR_CODE;
		break;
	}
	else
	{
		err_code = 0;
	}
  };
  return sum;
}

int  pow(int a, int b, int& err_code) {
  int exponent = 1;
  int temp_code;
  for(int i = 0; i < b; i++) { // we need to check i because the exponent might be 8200 or higher
    exponent = multiply(exponent, a, temp_code);
	if (exponent == ILLEGAL_NUM || i == ILLEGAL_NUM || temp_code == ERR_CODE) 
	{
		err_code = ERR_CODE;
		break;
	}
	else
	{
		err_code = 0;
	}
  };
  return exponent;
}

int main(void) {
	int err_code = 0;
	int num = 0;
	num = pow(8200, 1, err_code); //invalid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;


	num = pow(5, 5, err_code); //valid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;

	num = multiply(2050, 5, err_code); //invalid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;

	num = multiply(2050, 3, err_code); //valid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;

	num = add(2050, 6150, err_code); //invalid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;

	num = add(2050, 3, err_code); //valid
	if (err_code == ERR_CODE)
		std::cout << ERR_MSG << std::endl;
	else
		std::cout << num << std::endl;

	getchar();
}