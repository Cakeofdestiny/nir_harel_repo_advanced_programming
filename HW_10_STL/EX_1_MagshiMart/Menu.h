#pragma once

#include "utils.h"
#include "Customer.h"
#include <vector>
#include <algorithm>
#include <map>

typedef enum mainMenuOption {REGISTER_CUSTOMER = 1, UPDATE_CUSTOMER, PRINT_BEST_CUSTOMER, EXIT} mainMenuOption;
typedef enum customerMenuOption {ADD_ITEMS = 1, REMOVE_ITEMS, BACK_TO_MENU} customerMenuOption;

using std::vector;
using std::map;

#define PAUSE_CMD "PAUSE"
#define EXIT_ITEM_MODIFICATIONS 0

class Menu
{
public:
	Menu(const vector<Item>& itemList);
	~Menu();

	void runMainMenu();

private:
	void _registerCustomer();
	void _updateCustomer();
	void _printBestCustomer();
	void _addItems(Customer& customer);
	void _removeItems(Customer& customer);
	void _printCustomerMenu() const;
	void _printMenu() const;

	vector<Item> _itemList;
	map<string, Customer> _customerMap;
};
