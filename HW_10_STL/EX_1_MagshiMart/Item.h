#pragma once
#include <iostream>
#include <string>
#include <algorithm>
#include "utils.h"

#define MIN_UNIT_PRICE 0
#define MIN_COUNT 1

using std::string;

class Item
{
public:
	Item(const string& name, const string& serialNumber, const double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//Getters
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;
	
	//Setters
	void setName(const string& name);
	void setSerialNumber(const string& serialNumber);
	void setCount(const int count);
	void setUnitPrice(const double unitPrice);
	
	friend std::ostream& operator<< (std::ostream& stream, const Item& other);

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
};