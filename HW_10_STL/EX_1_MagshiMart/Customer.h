#pragma once

#include "Item.h"
#include "utils.h"
#include <set>
#include <iostream>

using std::set;

class Customer
{
public:
	Customer();
	Customer(string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	bool operator<(const Customer& other) const;

	//Getters
	string getName() const;
	set<Item> getItems() const;

	//Setters
	void setName(const string& name);
	void setItems(const set<Item> items);

	void printShoppingCart() const;
private:
	string _name;
	set<Item> _items;
};
