#include"Menu.h"
#include <map>
#include <vector>
#include <iostream>

using std::map;
using std::vector;

int main()
{
	map<string, Customer> abcCustomers;
	const vector<Item> itemList = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("Bread","00003",8.9),
		Item("Chocolate","00004",7.0),
		Item("Cheese","00005",15.3),
		Item("Rice","00006",6.2),
		Item("Fish", "00008", 31.65),
		Item("Chicken","00007",25.99),
		Item("Cucumber","00009",1.21),
		Item("Tomato","00010",2.32)};

	Menu m(itemList);
	m.runMainMenu();

	return 0;
}