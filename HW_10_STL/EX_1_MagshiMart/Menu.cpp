#include "Menu.h"

Menu::Menu(const vector<Item>& itemList)
{
	this->_itemList = itemList;
}

Menu::~Menu() {}

void Menu::runMainMenu()
{
	unsigned int userChoice; //Unsigned int and not enum because it's simpler with cin that way
	do
	{
		clearConsole();
		this->_printMenu();
		cout << endl << "Your choice: ";
		getInputFromUser(userChoice);

		switch (userChoice)
		{
		case REGISTER_CUSTOMER:
			this->_registerCustomer();
			break;

		case UPDATE_CUSTOMER:
			this->_updateCustomer();
			break;
			
		case PRINT_BEST_CUSTOMER:
			this->_printBestCustomer();
			break;

		default:
			break;
		}
	} while (userChoice != EXIT);
}

void Menu::_updateCustomer()
{
	unsigned int userChoice; //Unsigned int and not enum because it's simpler with cin that way
	string customerName = "";
	Customer customer;
	
	clearConsole();
	cout << "Login screen" << endl << "------------" << endl << "Name: ";
	getInputFromUser(customerName);
	//If we can't find the user in the map, it means that he needs to register
	if (this->_customerMap.find(customerName) == this->_customerMap.end())
	{
		cout << "Looks like you aren't registered to Magshimart." << endl
			<< "Please register at the main menu." << endl;
		system(PAUSE_CMD); //Redirect him to register
	}
	else
	{
		customer = this->_customerMap[customerName]; //get a copy of customer
		do
		{
			clearConsole();
			cout << "Logged in | " << customer.getName() << endl << endl;
			customer.printShoppingCart();

			this->_printCustomerMenu();
			cout << endl << "Your choice: ";

			getInputFromUser(userChoice);

			switch (userChoice)
			{
			case ADD_ITEMS:
				this->_addItems(customer);
				break;

			case REMOVE_ITEMS:
				this->_removeItems(customer);
				break;

			default:
				break;
			}
		} while (userChoice != BACK_TO_MENU);
		this->_customerMap[customerName] = customer; //Update customer because we changed its local copy
	}
}

void Menu::_printBestCustomer()
{
	clearConsole();
	if (this->_customerMap.size() > 0)
	{
		//Get the best customer using std::max_element, and an anonymous function for comparisons
		auto bestCustomer = std::max_element(this->_customerMap.begin(), this->_customerMap.end(),
			[](const auto& p1, const auto& p2) { //auto is a pair of string and customer in this case
			return p1.second < p2.second; }); //compares the values using the overloaded ops

		cout << "Our best customer is " << bestCustomer->second.getName() << "." << endl
			<< "He spent $" << bestCustomer->second.totalSum() << " in our shop!" << endl;
	}
	else
	{
		cout << "We don't have any registered customers, so we can't pick the best one." << endl;
	}
	system(PAUSE_CMD);
}

void Menu::_registerCustomer()
{
	clearConsole();
	string name = "";
	cout << "Welcome to the registration system of Magshimart." << endl
		<< "Please enter your name: ";
	getInputFromUser(name);
	//Because map returns a pair of location and whether it was inserted, we can use it to check if the user was there
	if (!this->_customerMap.insert(std::make_pair(name, Customer(name))).second) 
	{
		cout << "Looks like you're already registered." << endl
			<< "Please update your details at the main menu." << endl;
		system(PAUSE_CMD);
	}
}

//Prints the items the user can buy, and adds an item if it is valid.
void Menu::_addItems(Customer& customer)
{
	unsigned int userChoice;
	do
	{
		clearConsole();
		cout << "The items you can buy are: (" << EXIT_ITEM_MODIFICATIONS << " to exit)" << endl;

		printContainer(this->_itemList);
		cout << endl << "Your choice: ";
		getInputFromUser(userChoice);

		//Print container scales from 1 to n, so we need to scale accordingly.
		if (userChoice >= 1 && userChoice <= this->_itemList.size())
		{
			customer.addItem(this->_itemList[userChoice - 1]); //add the item
		}
	} while (userChoice != EXIT_ITEM_MODIFICATIONS);
}

void Menu::_removeItems(Customer& customer)
{
	unsigned int userChoice;
	
	set<Item> items = customer.getItems();
	set<Item>::iterator itemToRemove = items.begin();

	do
	{
		clearConsole();
		items = customer.getItems();
		itemToRemove = items.begin(); //init to the beginning of the container because we'll need to advance it later

		customer.printShoppingCart();
		cout << endl << "Which item would you like to remove from your shopping cart (" 
			<< EXIT_ITEM_MODIFICATIONS << " to exit)? ";
		getInputFromUser(userChoice);

		if (userChoice >= 1 && userChoice <=  items.size()) 
		{
			std::advance(itemToRemove, userChoice - 1); //Advance to the appropriate item
			customer.removeItem(*itemToRemove); //remove the item
		}
	} while (userChoice != EXIT_ITEM_MODIFICATIONS);
}

void Menu::_printMenu() const
{
	cout << "Welcome to MagshiMart!" << endl
		<< REGISTER_CUSTOMER << ". To register a new customer" << endl
		<< UPDATE_CUSTOMER << ". To update an existing customer's details" << endl
		<< PRINT_BEST_CUSTOMER << ". To print the customer who pays the most" << endl
		<< EXIT << ". To exit" << endl;
}

void Menu::_printCustomerMenu() const
{
	cout << endl << "Options:" << endl
		<< ADD_ITEMS << ". Add items" << endl
		<< REMOVE_ITEMS << ". Remove items" << endl
		<< BACK_TO_MENU << ". Back to menu" << endl;
}