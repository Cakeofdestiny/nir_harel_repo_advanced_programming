#include "Customer.h"

Customer::Customer()
{
	this->_name = "";
}

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer() {}

double Customer::totalSum() const
{
	set<Item>::iterator it = this->_items.begin();
	double sum = 0;

	for (it; it != this->_items.end(); ++it)
	{
		sum += (it->totalPrice());
	}
	return sum;
}

void Customer::addItem(Item item)
{
	set<Item>::iterator it = this->_items.find(item);
	if (it != this->_items.end()) //If the item was found
	{
		item.setCount(it->getCount() + item.getCount()); //Sum item and existing item counts
		this->_items.erase(*it);
	}
	this->_items.insert(item); //and insert the new one
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator it = this->_items.find(item);
	if (it != this->_items.end()) //If the item was found
	{
		if (it->getCount() > MIN_COUNT)
		{
			item.setCount(it->getCount() - 1); //Decrement the count of the item
			this->_items.erase(*it);
			this->_items.insert(item);
		}
		else
		{
			this->_items.erase(*it); //if it only has 1 element, just erase it
		}
	} //if it wasn't found we don't need to do anything
}

bool Customer::operator<(const Customer& other) const
{
	return this->totalSum() < other.totalSum();
}

string Customer::getName() const
{
	return this->_name;
}

set<Item> Customer::getItems() const
{
	return this->_items;
}

void Customer::setName(const string & name)
{
	this->_name = name;
}

void Customer::setItems(const set<Item> items)
{
	this->_items = items;
}

void Customer::printShoppingCart() const
{
	if (this->_items.size() > 0)
	{
		cout << "You have the following items in your shopping cart:" << endl;
		printContainer(this->_items);
		cout << endl << "Total: " << this->totalSum() << endl;
	}
	else
	{
		cout << "Your shopping cart is empty." << endl;
	}
}


