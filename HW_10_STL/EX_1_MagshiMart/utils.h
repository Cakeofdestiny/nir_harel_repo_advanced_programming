#pragma once

#include <exception>
#include <iostream>
#define CLEAR_CMD "CLS"

using std::exception;
using std::cin;
using std::endl;
using std::cout;

#define	ILLEGAL_UNIT_PRICE_ERR "Illegal unit price: Must be higher than 0."
#define ILLEGAL_COUNT_ERR "Illegal count: Must be higher than 0."

void clearConsole();

class IllegalUnitPrice : public exception
{
	virtual const char* what();
};

class IllegalCount : public exception
{
	virtual const char* what();
};

template<class T> //a generic template function to allow us to use any type of input with a single declaration
void getInputFromUser(T & val)
{
	bool cinFail = false;
	do
	{
		cin >> val;
		cinFail = cin.fail(); //store fail value

		cin.clear();
		cin.ignore(INT_MAX, '\n'); //Clear and flush cin incase the user entered a wrong values
	} while (cinFail);
};

//The function will print any container beautifully using any overloaded operators of type T.
template <typename T, template <class...> class Container_t>
void printContainer(const Container_t<T>& items)
{
	unsigned int ctr = 1;
	//Iterate over the container and print the item each time.
	for (auto it = items.begin(); it != items.end(); ++it)
	{
		cout << ctr << ". " << *it << endl;
		ctr++;
	}
}