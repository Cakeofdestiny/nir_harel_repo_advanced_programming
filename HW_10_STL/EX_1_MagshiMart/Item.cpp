#include "Item.h"

Item::Item(const string& name, const string& serialNumber, const double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	if (unitPrice <= MIN_UNIT_PRICE)
	{
		throw(IllegalUnitPrice());
	}
	else
	{
		this->_unitPrice = unitPrice;
	}
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

Item::~Item() {}
//Returns the count of the item times the unit price
double Item::totalPrice() const
{
	return this->_unitPrice * this->_count;
}

bool Item::operator<(const Item & other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return this->_serialNumber == other._serialNumber;
}

string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

void Item::setName(const string & name)
{
	this->_name = name;
}

void Item::setSerialNumber(const string & serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::setCount(const int count)
{
	if (count < MIN_COUNT)
	{
		throw(IllegalCount());
	}
	this->_count = count;
}

void Item::setUnitPrice(double unitPrice)
{
	if (unitPrice <= MIN_UNIT_PRICE)
	{
		throw(IllegalUnitPrice());
	}
	this->_unitPrice = unitPrice;
}

std::ostream & operator<<(std::ostream & stream, const Item & other)
{
	stream << other._name << " |\tPrice: " << other._unitPrice << " | Count: " << other._count;
	return stream;
}
