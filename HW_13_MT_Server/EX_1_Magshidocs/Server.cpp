#include "pch.h"
#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

Server::Server()
{
	//SOCK_STREAM & IPPROTO_TCP for a TCP socket
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 
	this->_serverOnline = true;

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - Failed to create the socket");

	//Start scanning the message queue
	std::thread(&Server::_messageQueueHandler, this).detach();
}

Server::~Server()
{
	this->_serverOnline = false;
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (this->_serverOnline)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		_accept();
	}
}

void Server::_accept()
{
	//Note that accept is blocking
	//this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread(&Server::_clientHandler, this, client_socket).detach();
}


void Server::_clientHandler(SOCKET clientSocket)
{
	bool clientExit = false;
	User user;
	try
	{
		TRACE("Pushing a new user to queue - ")
		std::lock_guard<mutex> lock(this->_userDequeMutex);
		//Try to initialize the user and then enqueue it
		user = User(clientSocket);
		TRACE("name: %s", user.getUsername().c_str())
		this->_userDeque.push_back(user); 
	}
	catch (const std::exception& e)
	{
		TRACE("Failed")
		clientExit = true;
		closesocket(clientSocket);
	}

	if (!clientExit)
	{
		//Send a 101 message to the new user
		this->_updateUsers(this->_userDeque.size() - 1);
	}

	//Wait on messages and exit if the client requests so
	while (this->_serverOnline && !clientExit)
	{
		userMessage message;
		try
		{
			//We can use this line because this function is blocking
			message = user.getMessage();
		}
		catch (const std::exception& e)
		{
			//We'll just set this message to client exit, and the message thread will handle the rest
			message.messageCode = MT_CLIENT_EXIT;
		}

		//Check if the client wants to exit
		clientExit = message.messageCode == MT_CLIENT_EXIT;
		
		//I added the extra scope to make sure that the lock guard is freed before calling the cond var
		{
			//And add the message to the queue
			std::lock_guard<mutex> lock(this->_msgQueueMutex);
			this->_msgQueue.push(std::make_pair(user, message));
		}
		this->_msgQueueCondVar.notify_one();
	}
}

void Server::_messageQueueHandler()
{
	std::pair<User, userMessage> clientMessage;
	while (this->_serverOnline)
	{
		std::unique_lock<mutex> lck(this->_msgQueueMutex);
		//Wait on the condition variable, and when we wake up check if there is anything in the message queue
		this->_msgQueueCondVar.wait(lck, [this]{return this->_msgQueue.size() != 0;});
		//Get the message from the queue and pop it
		clientMessage = this->_msgQueue.front();
		this->_msgQueue.pop();
		//We don't really need the message queue anymore, so we can unlock its mutex.
		lck.unlock();
		
		TRACE("%d from %s", clientMessage.second.messageCode, clientMessage.first.getUsername().c_str())

		if (clientMessage.second.messageCode == MT_CLIENT_EXIT)
		{
			std::lock_guard<mutex> lck(this->_userDequeMutex);
			//Close the socket and then remove from the list
			closesocket(clientMessage.first.getSocket());
			TRACE("Removing %s from the server", clientMessage.first.getUsername().c_str())
				//Remove the user from the deque if it matches the user in our client message
				this->_userDeque.erase(std::remove_if(
					this->_userDeque.begin(),
					this->_userDeque.end(),
					[clientMessage](const User& u) {return u == clientMessage.first; }));
		}
		//Check if it either finished editing or is currently editing
		else if (clientMessage.second.messageCode == MT_CLIENT_FINISH || clientMessage.second.messageCode == MT_CLIENT_UPDATE)
		{
			std::lock_guard<mutex> lck(this->_userDequeMutex);
			//Make sure that there's something in the queue
			if (this->_userDeque.size() > 0)
			{
				//Check if the currentuser is the one that is editing
				if (clientMessage.first == *this->_userDeque.begin())
				{
					if (clientMessage.second.messageCode == MT_CLIENT_FINISH)
					{
						//Push the first user to the end of the deque
						this->_userDeque.push_back(*(this->_userDeque.begin()));
						//And remove it from the beginning
						this->_userDeque.pop_front();
					}
					this->_updateSharedFile(clientMessage.second.fileContent);
				}
			}
		}
		//Update all users after the operation - 0 will start at the beginning
		this->_updateUsers(0);
	}
}

void Server::_updateUsers(int index)
{
	std::lock_guard<mutex> lck(this->_userDequeMutex);
	string currentUsername = "";
	string nextUsername = "";
	std::stringstream buffer;
	{   //A new scope so that the mutex will release at the end
		std::lock_guard<mutex> file_lock(this->_fileMutex);
		std::ifstream shared_file(SHARED_FILE_NAME);
		if (shared_file.is_open())
		{
			//Read into a string buffer
			buffer << shared_file.rdbuf();
			shared_file.close();
		}
	}	
	
	//If there's a value in the queue
	if (this->_userDeque.size() >= 1)
	{
		currentUsername = this->_userDeque.begin()->getUsername();
		//If there are at least two users in the queue - otherwise we might produce an error
		if (this->_userDeque.size() >= 2)
		{
			nextUsername = (this->_userDeque.begin() + 1)->getUsername();
		}
	}
	for (size_t i = index; i < this->_userDeque.size() && i >= 0; i++)
	{
		//Add a one to index, because program indexes are zero indexed
		std::cout << this->_userDeque[i].getUsername() << ": ";
		Helper::sendUpdateMessageToClient(this->_userDeque[i].getSocket(), buffer.str(), currentUsername, nextUsername
			, i+1);
	}
}

void Server::_updateSharedFile(string content)
{
	std::lock_guard<mutex> lck(this->_fileMutex);
	std::ofstream shared_file(SHARED_FILE_NAME);
	if (shared_file.is_open())
	{
		shared_file << content;
	}
}
