#pragma once
#include "exceptions.h"

#define SEND_ERR "I encountered an error while sending a message to the client."
#define RECV_ERR "I encountered an error while receiving a message from the client: "

#define TYPE_CODE_LEN 3
#define USERNAME_LEN 2
#define CONTENT_LEN 5

using std::string;

enum MessageType : byte
{
	MT_CLIENT_LOG_IN = 200,
	MT_CLIENT_UPDATE = 204,
	MT_CLIENT_FINISH = 207,
	MT_CLIENT_EXIT = 208,
	MT_SERVER_UPDATE = 101,
};

class Helper
{
public:
	static int getMessageTypeCode(SOCKET sc);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum, int flags);
	static void sendData(SOCKET sc, std::string message);
	static void sendUpdateMessageToClient(SOCKET sc, std::string fileContent, std::string currUser, std::string nextUser, int position);
	static std::string getPaddedNumber(int num, int digits);

private:
	static std::unique_ptr<char[]> getPartFromSocket(SOCKET sc, int bytesNum);
	static std::unique_ptr<char[]> getPartFromSocket(SOCKET sc, int bytesNum, int flags);
};


#ifdef _DEBUG 
#include <stdio.h> 
// Basically printf with a newline
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#else // Override trace to do nothing in any version other than Debug
#define TRACE(msg, ...)
#endif