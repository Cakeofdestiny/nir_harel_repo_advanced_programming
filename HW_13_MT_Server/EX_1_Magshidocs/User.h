#pragma once

#define USER_LOGIN_ERR "Error - User failed to log in correctly."
#define USER_BYTES 2
#define FILE_LEN_BYTES 5

struct userMessage
{
	int messageCode = 0;
	int fileContentSize = 0;
	string fileContent = "";
};


//Defines a class that will be used to store data about a user and communicate with it.
class User
{
public:
	//Uses the RAII idiom. Note that the server still has to send a response back.
	User(SOCKET socket);
	User();
	bool operator==(const User& other) const;

	string getUsername() const;
	SOCKET getSocket() const;
	//A blocking function that returns a userMessage struct with message details
	userMessage getMessage() const;
private:
	string _username;
	SOCKET _socket;
};