#include "pch.h"
#include "Helper.h"

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, TYPE_CODE_LEN);
	std::string msg(chars.get());

	if (msg == "")
	{
		return 0;
	}

	int res = std::atoi(chars.get());
	return res;
}

// Send a 101 (update message) to a specific client
void Helper::sendUpdateMessageToClient(SOCKET sc, string fileContent, string currUser, string nextUser, int position)
{
	string res;

	string code = std::to_string(MT_SERVER_UPDATE);
	string currFileSize = getPaddedNumber(fileContent.size(), CONTENT_LEN);
	string currUserSize = getPaddedNumber(currUser.size(), USERNAME_LEN);
	string nextUserSize = getPaddedNumber(nextUser.size(), USERNAME_LEN);

	res = code + currFileSize + fileContent + currUserSize + currUser + nextUserSize + nextUser + std::to_string(position);

	TRACE("Sending update message to client: %d, currUser=%s, nextUser=%s, position=%d", sc, currUser.c_str(), nextUser.c_str(), position);
	sendData(sc, res);
}

// receive data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, bytesNum, MSG_WAITALL);
	return atoi(chars.get());
}

// receive data from socket according to byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, bytesNum, 0);
	string res(chars.get());
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}

// recieve data from socket according byteSize
// this is private function
std::unique_ptr<char[]> Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

std::unique_ptr<char[]> Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{	
		return std::unique_ptr<char[]>(new char[1]());
	}

	std::unique_ptr<char[]> data(new char[bytesNum + 1]());
	int res = recv(sc, data.get(), bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = RECV_ERR;
		s += std::to_string(sc);
		throw socketRecvError(s.c_str());
	}

	data[bytesNum] = NULL; //Make sure that data ends with a null terminator
	return data;
}

// send data to socket
// Private funcs
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw socketSendError(SEND_ERR);
	}
}