#pragma once
#include "User.h"

#define SHARED_FILE_NAME "shared_file.txt"
#define FILE_LENGTH_CAP 99999

using std::queue;
using std::mutex;
using std::deque;
using std::condition_variable;

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void _accept();
	void _clientHandler(SOCKET clientSocket);
	void _messageQueueHandler();
	//Send a 101 message to users following the index (inclusive)
	void _updateUsers(int index);
	void _updateSharedFile(string content);

	bool _serverOnline;
	SOCKET _serverSocket;

	deque<User> _userDeque;
	mutex _userDequeMutex;

	queue<std::pair<User, userMessage>> _msgQueue;
	mutex _msgQueueMutex;
	condition_variable _msgQueueCondVar;

	mutex _fileMutex;
};

