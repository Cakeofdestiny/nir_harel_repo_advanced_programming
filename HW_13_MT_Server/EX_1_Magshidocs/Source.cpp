#include "pch.h"
#include "Helper.h"
#include "Server.h"
#include "WSAInitializer.h"

#define SERVER_PORT 8826
#define OK 0
#define COULDNT_START_WSA_EXCEPTION "I encountered an exception that I couldn't resolve: " 


int main()
{
	try
	{
		WSAInitializer wsa;
		Server server;
		server.serve(SERVER_PORT);
	}
	catch (const std::exception& e)
	{
		std::cout << COULDNT_START_WSA_EXCEPTION << e.what() << std::endl;
	}
	
	system("PAUSE");
	return OK;
}