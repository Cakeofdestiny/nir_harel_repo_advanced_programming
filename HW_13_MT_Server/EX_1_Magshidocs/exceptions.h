#pragma once
#include <exception>

class socketRecvError : public std::exception
{
public:
	socketRecvError(const char* what) : std::exception(what) {}
	virtual const char* what() const
	{
		return std::exception::what();
	}
};

class socketSendError : public std::exception
{
public:
	socketSendError(const char* what) : std::exception(what) {}
	virtual const char* what() const
	{
		return std::exception::what();
	}
};