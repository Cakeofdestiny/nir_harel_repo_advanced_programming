#include "pch.h"
#include "User.h"


User::User(SOCKET socket)
{
	this->_socket = socket;
	if (Helper::getMessageTypeCode(socket) == MT_CLIENT_LOG_IN)
	{
		//Get the username for as many bytes as the client specified
		this->_username = Helper::getStringPartFromSocket(socket,
			Helper::getIntPartFromSocket(socket, USER_BYTES), MSG_WAITALL);
	}
	else
	{
		throw std::exception(USER_LOGIN_ERR);
	}
}

User::User()
{
	this->_socket = NULL;
	this->_username = "";
}

bool User::operator==(const User & other) const
{
	//Even though socket is just an identifier, it should be somewhat unique and help us differentiate
	//between clients with the same username.
	return this->_username == other._username
		&& this->_socket == other._socket;
}

string User::getUsername() const
{
	return this->_username;
}

SOCKET User::getSocket() const
{
	return this->_socket;
}

userMessage User::getMessage() const
{
	userMessage responseStruct;
	int messageCode = Helper::getMessageTypeCode(this->_socket);
	responseStruct.messageCode = messageCode;
	if (messageCode == MT_CLIENT_UPDATE || messageCode == MT_CLIENT_FINISH)
	{
		responseStruct.fileContentSize = Helper::getIntPartFromSocket(this->_socket, FILE_LEN_BYTES);
		responseStruct.fileContent = Helper::getStringPartFromSocket(this->_socket, responseStruct.fileContentSize, MSG_WAITALL);
	}
	
	return responseStruct;
}
