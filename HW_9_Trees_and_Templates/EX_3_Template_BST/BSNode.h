#ifndef BSNode_H
#define BSNode_H

#include <iostream>
#define DEFAULT_DEPTH -1

using namespace std;

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
};


template<class T>
BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

template<class T>
BSNode<T>::BSNode(const BSNode & other)
{
	*this = other; //Use the overloaded assignment operator for this
}

template<class T>
BSNode<T>::~BSNode()
{  //postorder traversal
	delete this->_left;  //It's safe to delete the node even if it's a nullptr, because the delete command checks for that case.
	delete this->_right; //Recursive deletion of left and right. The data variable should be deleted without special intervention automatically because it isn't a ptr.
}

template<class T>
void BSNode<T>::insert(T value) //Inserts a value into the BST
{
	BSNode** target = nullptr;
	if (value == this->_data) //If it's the same value, increment count
	{
		this->_count++;
	}
	else //If the value is different
	{
		target = &(value > this->_data ? this->_right : this->_left); //Refer to the target with a **
		if ((*target) == nullptr) //If this node didn't previously have a node on the desired side
		{
			*target = new BSNode(value); //Define it as a node and set it there
		}
		else
		{
			(*target)->insert(value);
		}
	}
}

template<class T>
BSNode<T>& BSNode<T>::operator=(const BSNode & other)
{
	if (this == &other)
	{
		return *this; //No need to copy onto ourselves
	}
	delete this->_left; //It's safe to delete the node even if it's a nullptr, because the delete command checks for that case.
	delete this->_right;

	this->_left = nullptr;
	this->_right = nullptr;

	if (other._left) //Recursively copy the other nodes, if they exist.
	{
		this->_left = new BSNode(*other._left);
	}

	if (other._right)
	{
		this->_right = new BSNode(*other._right);
	}

	this->_count = other._count;
	this->_data = T(other._data); //Use a copy constructor to copy the data

	return *this;
}

//Some getter funcs
template<class T>
bool BSNode<T>::isLeaf() const
{
	return this->_left == nullptr && this->_right == nullptr;
}

template<class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template<class T>
BSNode<T> * BSNode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
BSNode<T> * BSNode<T>::getRight() const
{
	return this->_right;
}

template<class T>
int BSNode<T>::getCount() const
{
	return this->_count;
}

//Return whether the value is in the BST
template<class T>
bool BSNode<T>::search(T value) const
{
	bool found = this->_data == value;
	BSNode<T>* target = value > this->_data ? this->_right : this->_left; //Point to the next appropriate node

	if (!found)
	{
		if (target != nullptr)  //If there is a node at the next appropriate node, search there
		{
			found = target->search(value);

		}
	}
	return found;
}

//Get the height (basically depth of the deepest node) of the tree
template<class T>
int BSNode<T>::getHeight() const
{
	int heightLeft = 0, heightRight = 0;
	if (this->_left) //If theres a node on the left
	{
		heightLeft = this->_left->getHeight() + 1;
	}

	if (this->_right)
	{
		heightRight = this->_right->getHeight() + 1;

	}
	return heightLeft > heightRight ? heightLeft : heightRight; //Return the larger of the two
}

//Get the depth of any value within the bst, provided with the root
template<class T>
int BSNode<T>::getDepth(const BSNode<T> & root) const
{
	int depth = DEFAULT_DEPTH;

	BSNode<T>* target = this->_data > root.getData() ? root.getRight() : root.getLeft(); //Point to the next appropriate node

	if (target == this) //If this is the target
	{
		depth = 1;
	}
	else
	{
		if (target) //If there is a target at the appropriate branch
		{
			depth = this->getDepth(*target);
			depth = depth != DEFAULT_DEPTH ? depth + 1 : depth; //If it isn't the default depth, increment it.
		}
	}
	return depth;
}

//Traverse the tree (from this node) in an inorder function to alphabetically print the words
template<class T>
void BSNode<T>::printNodes() const
{
	if (this->getLeft())
	{
		this->getLeft()->printNodes();
	}

	std::cout << this->_data << " " << this->_count << endl;

	if (this->getRight())
	{
		this->getRight()->printNodes();
	}
}

#endif