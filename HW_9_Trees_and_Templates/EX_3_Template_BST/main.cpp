#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#define ARR_SIZE 15
using std::cout;
using std::endl;

#define searchPrint(bs, value) cout << "Is " << value << " in the tree?" << bs->search(value) << endl;

template <class T>
void printArr(T arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

template <class T>
BSNode<T> populateTree(T* arr, int size)
{
	BSNode<T> root(arr[0]);
	for(int i = 1; i<size ; i++)
	{
		root.insert(arr[i]);
	}
	return root;
}

int main()
{
	int intArr[ARR_SIZE] = {0xdeadbeef, 0x8badf00d, 19, 39, 48, 87, 666, 0xBAAAAAAD, 0xDABBAD0, -1, 3, 12, 18, 8, 23434523};
	string stringArr[ARR_SIZE] = {"we", "wish", "you", "a", "merry", "christmas", "we", "wish", "you", "a", "merry", "christmas", "and", "happy", "newyear"};

	BSNode<int> intTree = populateTree(intArr, ARR_SIZE);
	BSNode<string> stringTree = populateTree(stringArr, ARR_SIZE);

	printArr(intArr, ARR_SIZE);
	printArr(stringArr, ARR_SIZE);

	intTree.printNodes();
	stringTree.printNodes();

	system("pause");

	return 0;
}

