#include "Engine.h"

Engine::Engine()
{
	this->_hp = 0;
}

Engine::Engine(double hp)
{
	this->_hp = hp;
}

Engine::~Engine() {}

Engine::Engine(const Engine & other)
{
	*this = other;
}

Engine & Engine::operator=(const Engine & other)
{
	this->_hp = other._hp;
	return *this;
}

bool Engine::operator>(const Engine & other)
{
	return this->_hp > other._hp;
}

bool Engine::operator<(const Engine & other)
{
	return this->_hp < other._hp;
}

std::ostream & operator<<(std::ostream & stream, const Engine & other)
{
	stream << "Engine{hp=" << other._hp << "}";
	return stream;
}
