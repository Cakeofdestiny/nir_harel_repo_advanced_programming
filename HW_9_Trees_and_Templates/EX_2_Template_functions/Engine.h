#pragma once

#include <iostream>

class Engine
{
public:
	Engine();
	Engine(double hp);
	~Engine();

	Engine(const Engine& other);
	
	Engine& operator=(const Engine& other);
	bool operator>(const Engine& other);
	bool operator<(const Engine& other);
	friend std::ostream& operator<< (std::ostream& stream, const Engine& other);

private:
	double _hp;
};