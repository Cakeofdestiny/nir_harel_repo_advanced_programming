#include "functions.h"
#include <iostream>
#include "Engine.h"

#define MIN_ARR_SIZE 4
#define MIN_SIZE_MSG "Min size of the array is 4"
#define SIZE 5

using std::cerr;
using std::cout;
using std::endl;

template <class T>
void checkTemplateFuncs(T arr[], int arrSize);

int main() {
	double doubleArr[SIZE] = { 1.1, 2.9, -1.932, -1.932, 19 };
	char charArr[SIZE] = { 'a', 'e', 'e', 'd', 'x'};
	Engine engineArr[SIZE] = { Engine(5), Engine(5), Engine(3), Engine(7), Engine(16) };

	checkTemplateFuncs(doubleArr, SIZE);
	checkTemplateFuncs(charArr, SIZE);
	checkTemplateFuncs(engineArr, SIZE);
	
	system("pause");
	return 1;
}

//I: an array of sample values, min size 4
template <class T>
void checkTemplateFuncs(T arr[], int arrSize)
{
	if (arrSize < MIN_ARR_SIZE)
	{
		cerr << MIN_SIZE_MSG << endl;
		return;
	}
	//check compare
	cout << "I'm going to compare between the following objects (0 and 1, 1 and 2, 2 and 3):" << std::endl;
	printArray(arr, MIN_ARR_SIZE);
	cout << "--------------" << endl << "Comparisons:" << endl;

	std::cout << compare(arr[0], arr[1]) << std::endl; 
	std::cout << compare(arr[1], arr[2]) << std::endl;
	std::cout << compare(arr[2], arr[3]) << std::endl;
	
	//check printArray
	std::cout << "The correct print is the original array with endlines:" << std::endl;
	printArray(arr, arrSize);
	std::cout << std::endl;

	bubbleSort(arr, arrSize);
	//check bubbleSort
	std::cout << "The correct print is the sorted array (w/o endlines):" << std::endl;
	for (int i = 0; i < arrSize; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}