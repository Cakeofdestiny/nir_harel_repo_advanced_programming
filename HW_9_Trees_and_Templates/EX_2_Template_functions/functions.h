#pragma once

#define LARGER -1
#define SAME 0
#define SMALLER 1

//returns values according to the defines
template <class T>
int compare(T var1, T var2)
{
	int result = SAME;
	if (var2 < var1)
	{
		result = LARGER;
	}
	else if (var1 < var2)
	{
		result = SMALLER;
	}
	return result;
}

//I: array, array size
//Sorts the array using bubble sort
template <class T>
void bubbleSort(T arr[], int n)
{
	bool swapped = false;
	T temp;

	do
	{
		swapped = false;
		for (int i = 1; i < n; i++)
		{
			if (arr[i-1] > arr[i])
			{
				temp = arr[i - 1];
				arr[i-1] = arr[i];
				arr[i] = temp;
				swapped = true;
			}
		}
		
	} while (swapped);
}


//I: arr, array size
//Prints the elements of the array using the overloaded stream << op
template <class T>
void printArray(T arr[], int arrSize)
{
	for (int i = 0; i < arrSize; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}