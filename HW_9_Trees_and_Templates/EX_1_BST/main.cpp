#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#pragma comment(lib, "./printTreeToFile.lib")
#include "printTreeToFile.h"
using std::endl;
using std::cout;

#define searchPrint(bs, value) cout << "Is " << value << " in the tree?" << bs.search(value) << endl;

int main()
{
	string textTree = "BSTData.txt";

	BSNode wordBS("genetic");
	wordBS.insert("genetic");
	wordBS.insert("lifeform");
	wordBS.insert("lifeform");

	wordBS.insert("and");
	wordBS.insert("and");

	wordBS.insert("disk");
	wordBS.insert("disk");

	wordBS.insert("os");
	wordBS.insert("os");

	wordBS.printNodes();

	searchPrint(wordBS, "glados");

	printTreeToFile(&wordBS, textTree);
	ShellExecute(NULL, "open", "BinaryTree.exe", NULL, NULL, SW_SHOWDEFAULT);
	system("pause");
	system("taskkill /F /T /IM BinaryTree.exe"); //close the process

	BSNode bs("36");
	bs.insert("28");
	bs.insert("24");
	bs.insert("32");
	bs.insert("46");
	bs.insert("39");
	bs.insert("37");
	bs.insert("44");
	bs.insert("48");
	bs.insert("45");

	printTreeToFile(&bs, textTree);
	ShellExecute(NULL, "open", "BinaryTree.exe", NULL, NULL, SW_SHOWDEFAULT);

	cout << "Tree height: " << bs.getHeight() << endl;
	cout << "Depth of the node containing 44: " << bs.getRight()->getLeft()->getRight()->getDepth(bs) << endl;
	cout << "Depth of the node containing 32: " << bs.getLeft()->getRight()->getDepth(bs) << endl;

	cout << "Is 44 a leaf? " << bs.getRight()->getLeft()->getRight()->isLeaf() << endl;
	cout << "Is 39 a leaf? " << bs.getRight()->getLeft()->isLeaf() << endl;

	searchPrint(bs, "100");
	searchPrint(bs, "36");
	searchPrint(bs, "37");
	searchPrint(bs, "48");
	searchPrint(bs, "33");

	BSNode copiedBST(bs); //check copying

	bs.insert("Hello!"); //Add this to make sure that copiedBST is not dependant on bs

	cout << "BinaryTree.exe should not change in the next few seconds." << endl;

	Sleep(2000);

	printTreeToFile(&copiedBST, textTree);

	system("pause");
	system("taskkill /F /T /IM BinaryTree.exe"); //close the process
	remove(textTree.c_str());

	return 0;
}

