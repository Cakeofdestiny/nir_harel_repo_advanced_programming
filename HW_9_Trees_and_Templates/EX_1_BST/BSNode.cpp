#include "BSNode.h"

BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

BSNode::BSNode(const BSNode & other)
{
	*this = other; //Use the overloaded assignment operator for this
}

BSNode::~BSNode()
{  //postorder traversal
	delete this->_left;  //It's safe to delete the node even if it's a nullptr, because the delete command checks for that case.
	delete this->_right; //Recursive deletion of left and right. The data variable should be deleted without special intervention automatically because it isn't a ptr.
}

void BSNode::insert(string value) //Inserts a value into the BST
{
	BSNode** target = nullptr;
	if (value == this->_data) //If it's the same value, increment count
	{
		this->_count++;
	}
	else //If the value is different
	{
		target = &(value > this->_data ? this->_right : this->_left); //Refer to the target with a **
		if ((*target) == nullptr) //If this node didn't previously have a node on the desired side
		{
			*target = new BSNode(value); //Define it as a node and set it there
		}
		else
		{
			(*target)->insert(value);
		}
	}
}

BSNode & BSNode::operator=(const BSNode & other)
{
	if (this == &other) 
	{
		return *this; //No need to copy onto ourselves
	}
	delete this->_left; //It's safe to delete the node even if it's a nullptr, because the delete command checks for that case.
	delete this->_right;

	this->_left = nullptr;
	this->_right = nullptr;

	if (other._left) //Recursively copy the other nodes, if they exist.
	{
		this->_left = new BSNode(*other._left);
	}

	if (other._right)
	{
		this->_right = new BSNode(*other._right);
	}
	
	this->_count = other._count;
	this->_data = other._data; //Actually copy the count and data

	return *this;
}

//Some getter funcs

bool BSNode::isLeaf() const
{
	return this->_left == nullptr && this->_right == nullptr;
}

string BSNode::getData() const
{
	return this->_data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}

int BSNode::getCount() const
{
	return this->_count;
}

//Return whether the value is in the BST
bool BSNode::search(string value) const 
{
	bool found = this->_data == value;
	BSNode* target = value > this->_data ? this->_right : this->_left; //Point to the next appropriate node

	if (!found) 
	{
		if (target != nullptr)  //If there is a node at the next appropriate node, search there
		{
			found = target->search(value);
			
		}
	}
	return found;
}

//Get the height (basically depth of the deepest node) of the tree
int BSNode::getHeight() const
{
	int heightLeft = 0, heightRight = 0;
	if (this->_left) //If theres a node on the left
	{
		heightLeft = this->_left->getHeight() + 1;
	}

	if (this->_right)
	{
		heightRight = this->_right->getHeight() + 1;

	}
	return heightLeft > heightRight ? heightLeft : heightRight; //Return the larger of the two
}

//Get the depth of any value within the bst, provided with the root
int BSNode::getDepth(const BSNode & root) const 
{
	int depth = DEFAULT_DEPTH;
	
	BSNode* target = this->_data > root.getData() ? root.getRight() : root.getLeft(); //Point to the next appropriate node

	if (target == this) //If this is the target
	{
		depth = 1;
	}
	else
	{
		if (target) //If there is a target at the appropriate branch
		{
			depth = this->getDepth(*target); 
			depth = depth != DEFAULT_DEPTH ? depth + 1 : depth; //If it isn't the default depth, increment it.
		}
	}
	return depth;
}

//Traverse the tree (from this node) in an inorder function to alphabetically print the words
void BSNode::printNodes() const 
{
	if (this->getLeft())
	{
		this->getLeft()->printNodes();
	} 

	std::cout << this->_data << " " << this->_count << endl;

	if (this->getRight())
	{
		this->getRight()->printNodes();
	}
}
