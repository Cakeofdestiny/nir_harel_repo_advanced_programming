1.	a. INSERT INTO Users (NAME) Values ("Maria Curie");
	b. INSERT INTO Albums (Name, User_ID, CREATION_DATE) Values ("My Pictures", (SELECT Users.ID from Users WHERE Users.NAME == "Maria Curie"), "7/11/1923");
	c. INSERT INTO Pictures (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES 
	("Selfie", "Images/Selfie.png", "1/1/1907", 8),
	("Old Selfie", "Images/OldSelfie.png", "3/9/1920", 8),
	("My Home", "Images/Home.png", "3/4/2011", 8),
	("Childhood Selfie", "Images/Sweet16.png", "12/7/1883", 8);
	
	d. INSERT INTO Tags (PICTURE_ID, USER_ID) 
	SELECT Pictures.ID, ALBUMS.USER_ID FROM Pictures INNER JOIN Albums ON Pictures.ALBUM_ID = Albums.ID WHERE ALBUMS.ID == 8;

2.	UPDATE Users SET Name="DBG" Where Name=="David Ben-gurion";

3.	a. INSERT INTO Users (NAME) Values ("Elizabeth Windsor");
	b. INSERT INTO Albums (Name, User_ID, CREATION_DATE) Values ("Her Majesty", (SELECT Users.ID from Users WHERE Users.NAME == "Elizabeth Windsor"), "21/4/1926");
	c. INSERT INTO Pictures (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES 
	("Selfie", "Images/Selfie.png", "1/1/1907", 9),
	("Old Selfie", "Images/OldSelfie.png", "3/9/1920", 9),
	("My Home", "Images/Home.png", "3/4/2011", 9),
	("Stamp", "Images/Sweet16.png", "12/7/1883", 9);
	-- I'm not going to change these from Maria Curie, this is pointless
	d. INSERT INTO Tags (PICTURE_ID, USER_ID) 
	SELECT Pictures.ID, ALBUMS.USER_ID FROM Pictures INNER JOIN Albums ON Pictures.ALBUM_ID = Albums.ID WHERE ALBUMS.ID == 9;
	
4.	a. DELETE FROM Pictures WHERE Pictures.ID == (SELECT ID from Pictures WHERE NAME=="Stamp");
	This command will delete, however there is a foreign key constraint. This means that we must delete any
	foreign keys that are associated with the picture's ID.
	b. To do this, we can either alter the table to cascade the deletion when creating the constraint,
	or delete these programatically like this:
	
	DELETE FROM Tags WHERE Picture_ID == (SELECT ID from Pictures WHERE NAME=="Stamp");
	DELETE FROM Pictures WHERE Pictures.ID == (SELECT ID from Pictures WHERE NAME=="Stamp");
	