#pragma once
#include <list>
#include <string>

using std::string;

class Person; 
typedef std::list<Person> persons; 

struct Person
{
	int id;
	string firstName;
	string lastName;
	string email;
};