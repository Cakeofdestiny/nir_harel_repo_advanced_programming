#include "sqlite3.h"
#include "Person.h"
#include <iostream>
#include <memory>
#include <string>
#include <fstream>

using std::string;

//Will be called if it's a new DB
typedef void(*InitDBFunction)(sqlite3* db);

#define ACCESS_CHECK_EXISTENCE 0
#define OPEN_ERROR "Failed to open SQL Database with code: "
#define DB_FILENAME "Phonebook.sqlite"

#define CREATE_PERSON_TABLE "CREATE TABLE PERSON \
		(PersonID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		FIRST_NAME TEXT NOT NULL, \
		LAST_NAME TEXT NOT NULL, \
		EMAIL TEXT NOT NULL);"

#define PERSON_ID "ID"
#define PERSON_FIRST_NAME "FIRST_NAME"
#define PERSON_LAST_NAME "LAST_NAME"
#define PERSON_EMAIL "EMAIL"

#define CREATE_PHONE_PREFIX_TABLE "CREATE TABLE PhonePrefix \
		(PhonePrefixID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		Prefix TEXT NOT NULL);"

#define CREATE_PHONE_TABLE "CREATE TABLE Phone \
		(PhoneID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		PhonePrefixID INTEGER NOT NULL, \
		PhoneNumber TEXT NOT NULL, \
		PersonID INTEGER NOT NULL, \
		FOREIGN KEY (PersonID) REFERENCES PERSON(PersonID), \
		FOREIGN KEY (PhonePrefixID) REFERENCES PhonePrefix(PhonePrefixID));"

constexpr const char* phoneBookStatements[] = { CREATE_PERSON_TABLE, CREATE_PHONE_PREFIX_TABLE, CREATE_PHONE_TABLE };

void initializePhoneBook(sqlite3* db);
sqlite3* openDB(const char* filename, InitDBFunction initFunc = nullptr);

typedef int(*sqlCallback)(void* data, int argc, char** argv, char** azColName);

int executeSql(sqlite3* db, const char* statement, sqlCallback callback=nullptr, void* callbackData=nullptr);

int personCallback(void* data, int argc, char** argv, char** azColName);

void printPersons(const persons& personList);

int main()
{
	sqlite3* db = nullptr;

	try
	{
		db = openDB(DB_FILENAME, initializePhoneBook);
	}
	catch (int)
	{
		return -1;
	}

	//Insertion
	executeSql(db, "INSERT INTO PhonePrefix (PREFIX) VALUES \
	('02'), ('03'), ('04'), ('08'), ('09'), ('050'), ('052'), ('053'), ('054'), ('055'), ('073'), ('077')");

	//Insert people
	executeSql(db, "INSERT INTO Person (FIRST_NAME, LAST_NAME, EMAIL) VALUES \
	('Nir', 'Harel', 'nir@nirharel.space'), \
	('Guido', 'Van Rossum', 'guido@python.com'), \
	('Nir', 'Stallman', 'richard@stallman.org');");

	//Insert phone numbers
	executeSql(db, "INSERT INTO Phone (PhonePrefixID, PhoneNumber, PersonID) VALUES  \
		(0, '120', 1), \
		(1, '121', 1), \
		(2, '122', 1), \
		(3, '123', 2), \
		(4, '124', 2), \
		(5, '125', 3);");

	/* Commenting out lab 2 deletions because I don't wanna add a new person
	//Updates

	//Insert a wrong prefix
	executeSql(db, "INSERT INTO PhonePrefix (PREFIX) VALUES ('089')");
	//Correct it

	executeSql(db, "UPDATE PhonePrefix SET PREFIX='078' WHERE PREFIX='089';");
	executeSql(db, "UPDATE Person SET FIRST_NAME='Sonia', LAST_NAME='Elimelech' WHERE PersonID=1;");

	
	//Deletions
	//Delete the third phone number of the first user
	executeSql(db, "DELETE FROM Phone WHERE PhoneID == (SELECT PhoneID from Phone where PersonID==1 LIMIT 1 OFFSET 2);");
	//Delete the second user
	//First delete his phone numbers
	executeSql(db, "DELETE FROM Phone WHERE PersonID==2;");
	//And then actually delete the userz
	executeSql(db, "DELETE FROM Person WHERE PersonID==2;");

	*/

	persons personList;
	//Select people called nir
	executeSql(db, "SELECT * FROM Person WHERE FIRST_NAME == 'Nir';", personCallback, &personList);
	printPersons(personList);

	sqlite3_close(db);

	getchar();
	return 0;
}

//A regular sqlite callback. Handles entering person data into a c struct.
int personCallback(void* data, int argc, char** argv, char** azColName)
{
	Person p;
	persons* personList = (persons*)data;

	for (size_t i = 0; i < argc; i++)
	{
		string azColNameStr(azColName[i]);
		if (azColNameStr == PERSON_ID)
		{
			p.id = atoi(argv[i]);
		}else if (azColNameStr == PERSON_FIRST_NAME)
		{
			p.firstName = argv[i];
		}else if (azColNameStr == PERSON_LAST_NAME)
		{
			p.lastName = argv[i];
		}else if (azColNameStr == PERSON_EMAIL)
		{
			p.email = argv[i];
		}
	}

	personList->push_back(p);
	return 0;
}

//Input: sqlite3 db, sql statement, optional callback, optional callbackData
//Executes the statement with the parameters, and prints the error code, statement, and message upon failure.
int executeSql(sqlite3* db, const char* statement, sqlCallback callback, void* callbackData)
{
	int res = 0;
	char* errMessage = nullptr;
	res = sqlite3_exec(db, statement, callback, callbackData, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cerr << "Operation failed with error code " << res << ", message: " << errMessage << std::endl
			<< "While executing: " << statement << std::endl;
		sqlite3_free(errMessage);
	}
	return res;
}

//Prints a list of people
void printPersons(const persons & personList)
{
	for (const Person& p: personList)
	{
		std::cout << "First name: " << p.firstName << ", Last name: " << p.lastName << ", email: " << p.email << std::endl;
	}
}

//Initializes the phone book with the unique ptr
//May throw an int sql exception
void initializePhoneBook(sqlite3* db)
{
	char* errMsg = nullptr;
	int res = 0;
	//Use countof to statically check size of array
	for (size_t i = 0; i < _countof(phoneBookStatements); i++)
	{
		res = sqlite3_exec(db, phoneBookStatements[i], NULL, NULL, &errMsg);
		if (res != SQLITE_OK)
		{
			std::cerr << "Table creation failed with error code " << res << ", message: " << errMsg << std::endl;
			//Make sure to free the dynamically allocated error message
			sqlite3_free(errMsg);
			throw(res);
		}
	}
}

//Input: Filename of database file, callback function to init the database (optional)
//Output: sqlite3 ptr (that must be closed)
//Note: the function will raise an exception with the native sql error number if it fails.
sqlite3* openDB(const char* filename, InitDBFunction initFunc) 
{
	sqlite3* db = nullptr;
	
	bool fileExistedBeforeOpen = false;
	{
		std::ifstream infile(filename);
		fileExistedBeforeOpen = infile.good();
	}

	int res = sqlite3_open(filename, &db);
	if (res == SQLITE_OK)
	{
		//I could also use if not exists for this but oh well
		//If we opened it and it didn't exist before, initialize it
		if (!fileExistedBeforeOpen && initFunc != nullptr)
		{
			initFunc(db);
		}
	}
	else
	{
		std::cerr << OPEN_ERROR << res << std::endl;
		throw(res);
	}
	return db;
}
