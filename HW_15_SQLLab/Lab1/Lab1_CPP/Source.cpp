#include "sqlite3.h"
#include <io.h>
#include <iostream>
#include <memory>
#include <string>
#include <fstream>

using std::string;

//Will be called if it's a new DB
typedef void(*InitDBFunction)(sqlite3* db);

#define ACCESS_CHECK_EXISTENCE 0
#define OPEN_ERROR "Failed to open SQL Database with code: "
#define DB_FILENAME "Phonebook.sqlite"

#define CREATE_PERSON_TABLE "CREATE TABLE PERSON \
		(PersonID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		LAST_NAME TEXT NOT NULL, \
		FIRST_NAME TEXT NOT NULL, \
		EMAIL TEXT NOT NULL);"

#define CREATE_PHONE_PREFIX_TABLE "CREATE TABLE PhonePrefix \
		(PhonePrefixID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		Prefix TEXT NOT NULL);"

#define CREATE_PHONE_TABLE "CREATE TABLE Phone \
		(PhoneID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
		PhonePrefixID INTEGER NOT NULL, \
		PhoneNumber TEXT NOT NULL, \
		PersonID INTEGER NOT NULL, \
		FOREIGN KEY (PersonID) REFERENCES PERSON(PersonID), \
		FOREIGN KEY (PhonePrefixID) REFERENCES PhonePrefix(PhonePrefixID));"

constexpr const char* phoneBookStatements[] = { CREATE_PERSON_TABLE, CREATE_PHONE_PREFIX_TABLE, CREATE_PHONE_TABLE };

void initializePhoneBook(sqlite3* db);
sqlite3* openDB(const char* filename, InitDBFunction initFunc = nullptr);

int main()
{
	sqlite3* db = nullptr;
	try
	{
		db = openDB(DB_FILENAME, initializePhoneBook);
	}
	catch (int)
	{
		return -1;
	}

	sqlite3_close(db);

	return 0;
}

//Initializes the phone book with the unique ptr
//May throw an int sql exception
void initializePhoneBook(sqlite3* db)
{
	char* errMsg = nullptr;
	int res = 0;
	//Use countof to statically check size of array
	for (size_t i = 0; i < _countof(phoneBookStatements); i++)
	{
		res = sqlite3_exec(db, phoneBookStatements[i], NULL, NULL, &errMsg);
		if (res != SQLITE_OK)
		{
			std::cerr << "Table creation failed with error code " << res << ", message: " << errMsg << std::endl;
			//Make sure to free the dynamically allocated error message
			sqlite3_free(errMsg);
			throw(res);
		}
	}
}

//Input: Filename of database file, callback function to init the database (optional)
//Output: sqlite3 ptr (that must be closed)
//Note: the function will raise an exception with the native sql error number if it fails.
sqlite3* openDB(const char* filename, InitDBFunction initFunc) 
{
	sqlite3* db = nullptr;
	
	bool fileExistedBeforeOpen = false;
	{
		std::ifstream infile(filename);
		fileExistedBeforeOpen = infile.good();
	}

	int res = sqlite3_open(filename, &db);
	if (res == SQLITE_OK)
	{
		//I could also use if not exists for this but oh well
		//If we opened it and it didn't exist before, initialize it
		if (!fileExistedBeforeOpen && initFunc != nullptr)
		{
			initFunc(db);
		}
	}
	else
	{
		std::cerr << OPEN_ERROR << res << std::endl;
		throw(res);
	}
	return db;
}
