#include <cmath>

#define PI 3.14159265359

//1.This code violates OCP, because we'll need to modify the calc area function every time we want
//to add a new shape. 
//2.Additionally, the user will have to specify the type every time and this is error prone. Switch case is often a sign of violating the OCP.
//It's better to use polymorphism in order to implement this.
//4. This still violates the OCP because we'll need to modify CalcArea every time we want to add a new shape.
//It keeps calcArea open for expandability, but also open for unexpected changes.

//Shape interface
class IShape
{
public:
	virtual double calcArea() const = 0;
};

class Circle : IShape
{
public:
	double calcArea() const
	{
		return PI * std::pow(this->_radius, 2);
	}

private:
	int _x;
	int _y;
	int _radius;
};


class Rectangle : IShape
{
public:
	double calcArea() const
	{
		return (this->_x2 - this->_x1) * (this->_y2 - this->_y1);
	}

private:
	int _x1;
	int _y1;
	int _x2;
	int _y2;
};