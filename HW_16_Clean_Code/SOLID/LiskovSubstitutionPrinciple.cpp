#include <iostream>

//1. Nope. Even though "Instrument" is called that way, it doesn't represent any instrument.
//Instrument should be called "ChordedInstrument", because it forces you to implement playChord.
//In any case, a Drum is not a chorded instrument, so this violates inheritance rules.

//This code violates LSP, because throwing exceptions is essentially a part of the functions signature - i.e. expected
//behaviour. It doesn't allow for a quick insertion of new classes and usages because we can't know what will throw exceptions.

//Now that we separated it, Drum doesn't have to throw an exception (which is unexpected behaviour for Instrument).

class Instrument {};

class ChordedInstrument : public Instrument
{
public:
	virtual void playChord() const = 0;
};

class Guitar : public ChordedInstrument
{
public:
	void playChord() const
	{
		std::cout << "Am" << std::endl;
	}
};
class Drum : public Instrument {};
void Musician(const ChordedInstrument& instrument)
{
	instrument.playChord();
}