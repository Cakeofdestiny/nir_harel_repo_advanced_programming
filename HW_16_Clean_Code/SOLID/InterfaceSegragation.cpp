#include <iostream>
#include <stdexcept>

struct IPhone
{
	virtual void call() const = 0;
	virtual void sms() const = 0;
	virtual void fax() const = 0;
};

struct HomePhone : public IPhone
{
public:
	virtual void call()
	{
		std::cout << "E.T. Phone Home" << std::endl;
	}
	virtual void sms()
	{
		throw std::runtime_error("unsupported instrument");
	}
	virtual void fax()
	{
		std::cout << "E.T. Fax Home" << std::endl;
	}
};

//1. Smartphone implementation using a similar method as the student used

struct SmartPhone : public IPhone
{
public:
	virtual void call()
	{
		std::cout << "E.T. Phone Home" << std::endl;
	}
	virtual void sms()
	{
		std::cout << "E.T. SMS Home" << std::endl;
	}
	virtual void fax()
	{
		throw std::runtime_error("unsupported instrument");
	}
};

//2. Yes, and this is rather similar to the previous OCP principle and also LSP. 
//SmartPhone and HomePhone are forced to implement functions they don't need because the phone interface
//is too large and adds functions that many clients can't implement.

struct IPhoneable
{
	virtual void call() const = 0;
};

struct IMessagable
{
	virtual void sms() const = 0;
};

struct IFaxable
{
	virtual void fax() const = 0;
};

struct smartPhoneRevised : IMessagable, IPhoneable
{
	virtual void call()
	{
		std::cout << "E.T. Phone Home" << std::endl;
	}
	virtual void sms()
	{
		std::cout << "E.T. SMS Home" << std::endl;
	}
};

struct HomePhoneRevised: public IPhoneable, public IFaxable
{
public:
	virtual void call()
	{
		std::cout << "E.T. Phone Home" << std::endl;
	}
	virtual void fax()
	{
		std::cout << "E.T. Fax Home" << std::endl;
	}
};

//Just for the entry point
void main() {}