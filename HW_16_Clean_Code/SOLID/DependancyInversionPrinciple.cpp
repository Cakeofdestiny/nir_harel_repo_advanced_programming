#include <string>
#include <vector>

class Sendable
{
public:
	virtual void send() const = 0;
};

class Email : Sendable
{
public:
	Email()
	{
	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void send() const
	{
		// Send email
	}
};

class SMS : Sendable
{
public:
	SMS()
	{
	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void send() const
	{
		// Send SMS
	}
};

class Reminder
{
	std::vector<Sendable*> _sendables;
public:
	Reminder(std::vector<Sendable*> sendables,
		const std::string& from,
		const std::string& subject,
		const std::string& content) : _sendables(sendables)
	{
	}
	~Reminder()
	{
		for (Sendable* s: _sendables)
		{
			delete s;
		}
	}
	void sendReminder() const
	{
		for (Sendable* s: _sendables)
		{
			s->send();
		}
	}
};

//1. The code doesn't really work, but if we assume that it does, there's no need for 
//the dynamic memory managment for email. This is probably prep for polymorphis, though.
//2. No, it wasn't really easy because I had to also modify the Reminder class for it to work.
//If I wanted to add whatsapp messages I'd have to do this process all over again.