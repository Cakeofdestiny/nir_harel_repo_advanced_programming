#include <string>

#define EMAIL_MARKER '@'

//Yes, in my opinion this (i.e. the original) code violates the SRP principle. The class person is responsible
//both for handling email validation and storing details about a person. We can add a second class, email, that
//will be responsible for that.

class Email
{
public:
	Email(std::string email)
	{
		if (email.find(EMAIL_MARKER) == std::string::npos)
		{
			throw std::invalid_argument("email address not valid");
		}
		this->_email = email;
	}

	std::string getEmail() const
	{
		return this->_email;
	}
private:
	std::string _email;
};

class Person
{
	std::string _firstName;
	std::string _lastName;
	Email _email;
	
public:
	Person(const std::string& firstName, const std::string& lastName, const
		std::string& email)
		: _firstName(firstName), _lastName(lastName), _email(email) {}
};