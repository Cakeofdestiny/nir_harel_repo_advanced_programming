#include <cmath>
#include <vector>

struct Point
{
	int x;
	int y;
};

//Calc distance between two points using the Pythagorean theorem. 
//Yes, it'll be easier to find and fix bugs in this code, since the distance calculation only happens in 
//this function and not many times. The previous code was WESix times and this is DRY.
double calcDistanceBetweenPoints(const Point& A, const Point& B)
{
	return sqrt(pow(B.x - A.x, 2) + pow(B.y - A.y, 2));
}

//The bug is in the third line, where the y axis of C is substracted from... the y axis of C.
//Additionally, in the second line, they multiply C.y-B.y in A.y-B.y, which will not result in the expected 
//behaviour.
double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C)
{
	return calcDistanceBetweenPoints(A, B) + calcDistanceBetweenPoints(B, C) + calcDistanceBetweenPoints(C,A);
}

double calcDistanceInArrImproved(std::vector<Point> points)
{
	double dist = 0;
	//calc distance between first and second, second and third...
	for (size_t i = 0; i < points.size() - 1; i++)
	{
		dist += calcDistanceBetweenPoints(points[i], points[i + 1]);
	}
	//Calc distance between first and last
	dist += calcDistanceBetweenPoints(points[points.size() - 1], points[0]);
	return dist;
}

double calcPentagonPerimeter(const Point& A, const Point& B, const Point& C, const Point& D, const Point& E)
{
	//The previous solution is pretty DRY, but we can improve it by making a generic function to calculate the
	//perimeter of any polygon.
	return calcDistanceInArrImproved({ A,B,C,D,E });
}

