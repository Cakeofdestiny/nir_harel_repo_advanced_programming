#include <vector>


//The function receives a vector of floats, and calculates their average
float calcAverage(const std::vector<int>& nums)
{
	if (nums.empty())
		throw std::exception("invalid: cannot average an empty vector");
	float sum = 0.0;
	for (std::vector<int>::const_iterator it = nums.begin(), e = nums.end(); it != e; ++it)
	{
		sum += *it;
	}

	return sum / nums.size();
}