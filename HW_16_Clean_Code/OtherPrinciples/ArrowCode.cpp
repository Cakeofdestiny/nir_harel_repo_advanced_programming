#include <algorithm>
#include <string>

#define MIN_NAME_LENGTH 1
#define MAX_NAME_LENGTH 10

// ArrowCode.cpp
bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names
	return true;
}
bool isNotValid(char ch)
{
    return !((ch == '!' || ch == '$' || ch == '.') || (isalpha(ch) && isdigit(ch)));
}

//isValidUsername checks whether the name is within range. 
//Then it checks if the first char is alphabetic, and then if the name is alphanumeric for the rest
//of them. Finally, it checks if it's unique.
bool isValidUserName(const std::string& username)
{
	bool valid = false;

	if (!(username.length() >= MIN_NAME_LENGTH && username.length() <= MAX_NAME_LENGTH))
	{
		throw std::invalid_argument("The username is not of the appropriate length.");
	}

	if (!(isalpha(username[0]) && isdigit(username[1])))
	{
		throw std::invalid_argument("The username does not fit the digit and alphanumeric char predicate.");
	}

	bool foundNotValidChar = std::find_if(username.begin(),

		username.end(), isNotValid) != username.end();
	if (!foundNotValidChar)
	{
		valid = isUniqueName(username);
	}

	return valid;
}

//3. The main problem is deep nesting in the isUsernameValid function. It calls many other functions and has
//many ifs, which cause it to be difficult to maintain.