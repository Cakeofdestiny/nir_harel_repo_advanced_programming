#include "Digit.h"

Digit::Digit(char dig)
{
	this->setDig(dig);
}

short Digit::getDig() const
{
	return this->_dig;
}

void Digit::setDig(char dig)
{
	short res = dig - ZERO;
	if (res < 0 || res > 9)
	{
		throw std::exception(NONDIG_EXCEPTION);
	}
	else
	{
		this->_dig = res;
	}
}
