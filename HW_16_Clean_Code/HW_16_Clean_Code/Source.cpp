#include "Digit.h"
#include <string>

#define LOWER_BOUND 0 
#define UPPER_BOUND 9

int main()
{


	return 0;
}

//Gets a string and returns whether it's a proper num
//This is not simple. There's no need for the Digit class and exception handling here.
//It's just extra code that any developer would need to read.
bool isNum(std::string str) 
{
	bool isDig = true;
	if (str.size() > 0 && str[0] != ZERO)
	{
		try
		{
			for (char c : str)
			{
				Digit d(c);
			}
		}
		catch (const std::exception&)
		{
			isDig = false;
		}
	}
	else
	{
		isDig = false;
	}
	return isDig;
}

//Now, this is simpler. We simply perform the check here instead of creating an object and handling exceptions
//for some reason.
bool isNumRewritten(std::string str)
{
	bool isDig = true;
	if (str.size() > 0 && str[0] != ZERO)
	{
		for (size_t i = 0; i < str.size() && isDig; i++)
		{
			//If it's not within bounds
			if ((str[i] - ZERO) < LOWER_BOUND || (str[i] - ZERO) > UPPER_BOUND)
			{
				isDig = false;
			}
		}
	}
	else
	{
		isDig = false;
	}
	return isDig;
}

