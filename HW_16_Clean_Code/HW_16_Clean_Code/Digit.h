#pragma once

#include <exception>
#define ZERO '0'

#define NONDIG_EXCEPTION "Error: the char didn't represent a digit."

class Digit
{
public:
	Digit(char dig);

	short getDig() const;
	void setDig(char dig);

private:
	short _dig;
};

