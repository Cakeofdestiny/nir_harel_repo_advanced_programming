#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join(); //wait for t1 to end
}

//Simply prints a vector with one value for each line
void printVector(vector<int> primes)
{
	vector<int>::iterator it = primes.begin();
	for (it; it != primes.end(); ++it) //iterate over primes
	{
		cout << *it << endl;
	}
}

//Pushes all prime numbers into a vector
void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;
	for (i=begin; i < end; i++)
	{
		isPrime = true;
		for (j=2; j <= ceil(sqrt(i)) && isPrime; j++) //loop from 2 to sqrt of the number
		{
			if (i % j == 0)
			{
				isPrime = false; //if it can divide cleanly it isn't a prime
			}
		}
		if (isPrime)
		{
			primes.push_back(i);
		}
	}
}

//Call getPrimes using a thread
vector<int> callGetPrimes(int begin, int end)
{
	auto initialTime = high_resolution_clock::now();
	unsigned int duration = 0;

	vector<int> primeVector;
	thread primeT(getPrimes, begin, end, std::ref(primeVector));
	primeT.join(); //wait until execution finishes

	duration = std::chrono::duration_cast<milliseconds>
		(high_resolution_clock::now() - initialTime).count();

	cout << "It took me " << duration << " ms to calculate the primes from " << begin << " to " << end << "." << endl;
	return primeVector;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;
	for (i = begin; i < end; i++)
	{
		isPrime = true;
		for (j = 2; j <= ceil(sqrt(i)) && isPrime; j++) //loop from 2 to sqrt of the number
		{
			if (i % j == 0)
			{
				isPrime = false; //if it can divide cleanly it isn't a prime
			}
		}
		if (isPrime)
		{
			file << i << endl; //write to file
		}
	}
}

//begin, end, file path and the number of threads to execute
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	auto initialTime = high_resolution_clock::now();
	unsigned int duration = 0;

	ofstream file(filePath);
	thread* threadPool;
	int diff = ceil((end - begin) / (float) N); //calculate the difference (allocation for each thread)
	int rangeBegin = 0, rangeEnd = 0;

	if (!file.is_open())
	{
		cout << "I couldn't open '" << filePath << "'. Please check the file path.";
	}
	else
	{
		threadPool = new thread[N];
		for (int i = 0; i < N; i++)
		{
			rangeBegin = diff * i;
			rangeEnd = rangeBegin+diff;
			if (rangeEnd > end)
			{
				rangeEnd = end; //make sure end of range is not beyond the actual end
			}
			threadPool[i] = thread(writePrimesToFile, rangeBegin, rangeEnd, std::ref(file));
		}
		for (int i = 0; i < N; i++)
		{
			threadPool[i].join(); //wait until all threads finish
		}
		file.close();

		duration = std::chrono::duration_cast<milliseconds>
			(high_resolution_clock::now() - initialTime).count();

		cout << "It took me " << duration << " ms to calculate primes from "
			<< begin << " to " << end << ", on " << N << " threads." << endl;
	}
}
