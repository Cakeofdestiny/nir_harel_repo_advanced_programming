#include "Vector.h"

//Vector constructor
Vector::Vector(int n)
{
	if (n < MIN_VAL)
	{
		std::cerr << "Constructor must be called with at least the number " << MIN_VAL << ". " << MIN_VAL << " was used instead.";
		n = MIN_VAL;
	}
	this->_elements = new int[n]; //allocate an area for the int array
	this->_capacity = n; //set capacity to n
	this->_resizeFactor = n; 
}

//Copying constructor
Vector::Vector(const Vector & other)
{
	*this = other; //use our overloaded assignment operator for this
}

//Assignment operator
Vector & Vector::operator=(const Vector & other)
{
	if (this == &other) //If we're copying onto ourselves
	{
		return *this;
	}
	
	delete[] this->_elements; //free old memory

	//first shallow copy primitives
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size; //we can use these private fields since this is in the same class

	//now deep copy our array after defining some memory for it
	this->_elements = new int[this->_capacity];

	//iterate over the array
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

int & Vector::operator[](int n) const
{
	if (n >= this->_size)
	{
		n = 0; // set to first val as per the instructions
		std::cerr << OUT_OF_BOUNDS_ERR << std::endl;
	}
	return this->_elements[n];
}

//print the vector's values to a stream
void Vector::print_element_list(std::ostream & stream) const
{
	stream << "{"; 
	//iterate over the element list
	for (int i = 0; i < this->_size; i++)
	{
		
		stream << this->_elements[i]; //print the current element
		if (i+1 < this->_size) //if we're not at the last one, also print the line separator
		{
			stream << ", ";
		}
	}
	stream << "}";
}

//Destructor
Vector::~Vector()
{
	delete[] this->_elements; //Free our array's memory for our array
}

//GETTERS
int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

//Return true if the vector is empty
bool Vector::empty() const
{
	return this->_size == 0;
}

//Adds an element to the end of the vector
void Vector::push_back(const int & val)
{
	this->reserve(this->_size + 1); //Reserve size for size + 1
	//Put the value in the end of the vector
	this->_elements[this->_size] = val;
	//And mark its size as bigger
	this->_size++;
}

//Get an element from the end of the vector
int Vector::pop_back()
{
	int val = ERR_VAL;
	if (!this->empty()) //If not empty
	{
		val = this->_elements[this->_size - 1]; //Get the last value in the array
		this->_size--; //reduce size by 1
	}
	else
	{
		std::cerr << EMPTY_ERR; //Print error message to cerr
	}
	return val;
}

//Make sure we have enough capacity, and reserve with the appropriate resize factor.
void Vector::reserve(int n)
{
	int* new_arr = 0;
	int new_size = 0;
	int i = 0;
	if (n > this->_capacity) //Only reprovision if n is bigger than current capacity
	{
		//If we need to perform a more complex operation
		if (n % this->_resizeFactor != 0)
		{
			new_size = (n - (n % this->_resizeFactor)) + this->_resizeFactor; //Add resize factor to n
		}
		else
		{
			new_size = n; //if it divides cleanly just set the new size to n
		}
		
		new_arr = new int[new_size]; //define new arr

		//copy the old array to the new one
		for (int i = 0; i < this->_capacity; i++)
		{
			new_arr[i] = this->_elements[i];
		}

		delete[] this->_elements;  //free memory of old array
		this->_elements = new_arr; //define elements as the new array
		this->_capacity = new_size; //set capacity to new size
	}
}

//I: number for resize
//Resizes a vector according to this number.
void Vector::resize(int n)
{
	this->reserve(n);//first make sure that we have enough capacity
	this->_size = n; //Set new size to n
}

//I: a value
//sets all accessible values of the vector to `val`.
void Vector::assign(int val)
{
	int i = 0;
	//go over the vect
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;  //and assign the elements to our val parameter
	}
}

//Similar to resize, but set the new values to `val`.
void Vector::resize(int n, const int & val)
{
	int old_size = this->_size;
	int i = 0;

	this->resize(n);

	//iterate from the previously last element, to the currently last element.
	//If the size was decreased or it didn't change, this loop will just not run.
	for (i = old_size; i < this->_size; i++)
	{
		this->_elements[i] = val; //set new element to val
	}
}


//a stream op for printing
std::ostream & operator<<(std::ostream & stream, const Vector & vect)
{
	stream << "Vector Info" << std::endl << "-----------" << std::endl;
	stream << "Capacity: " << vect._capacity << std::endl;
	stream << "Size: " << vect._size << std::endl;
	stream << "Data: ";
	vect.print_element_list(stream);
	stream << std::endl;

	return stream;
}
