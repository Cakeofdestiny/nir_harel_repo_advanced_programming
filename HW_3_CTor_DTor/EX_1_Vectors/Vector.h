#ifndef VECTOR_H
#define VECTOR_H

#define MIN_VAL 2

#define ERR_VAL -9999
#define EMPTY_ERR "Error: Pop from empty vector"
#define OUT_OF_BOUNDS_ERR "Out of Bounds Error: Trying to access an inaccessible value"
#include <iostream>

class Vector
{
private:
	//Fields
	int* _elements;
	int _capacity; //Total memory allocated
	int _size; //Size of vector to access
	int _resizeFactor; // how many cells to add when need to reallocate
public:
	
	Vector(int n);
	~Vector();
	int size() const;//return size of vector
	int capacity() const;//return capacity of vector
	int resizeFactor() const; //return vector's resizeFactor
	bool empty() const; //returns true if size = 0

	//Modifiers
	void push_back(const int& val);//adds element at the end of the vector
	int pop_back();//removes and returns the last element of the vector
	void reserve(int n);//change the capacity
	void resize(int n);//change _size to n, unless n is greater than the vector's capacity
	void assign(int val);//assigns val to all elemnts
	void resize(int n, const int& val);//same as above, if new elements added their value is val

	//The big three (d'tor is above)
	Vector(const Vector& other);
	Vector& operator=(const Vector& other);

	//Element Access
	int& operator[](int n) const;//n'th element

	//add a tostring operator
	friend std::ostream& operator<< (std::ostream& stream, const Vector& matrix);

	//Print the list of elements to an ostream.
	void print_element_list(std::ostream& stream) const;

};

#endif