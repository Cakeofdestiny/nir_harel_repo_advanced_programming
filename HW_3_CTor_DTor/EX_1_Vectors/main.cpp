#include "Vector.h"

using std::cout;
using std::endl;

int main()
{
	Vector vect(7);
	vect.push_back(1);
	vect.push_back(2);
	vect.push_back(3);
	vect.push_back(4);
	vect.push_back(5);

	cout << vect << endl;


	vect.push_back(1);
	vect.push_back(2);
	vect.assign(5);

	cout << "both should be 5:" << endl;
	cout << vect << endl;

	vect.resize(10, 0);

	cout << vect << endl;

	vect[3] = 50;
	vect[4] = 51;
	vect[5] = 52;
	vect[6] = 53;
	cout << vect << endl;

	vect.resize(20, 0);
	cout << vect << endl;

	vect[100] = 13; //should print an error message

	cout << "testing copying stuff \n";

	Vector vect2 = vect;
	cout << vect2;

	vect2[3] = 999;
	cout << vect2;
	cout << vect;

	Vector vect3(17);
	vect3.resize(5);
	vect3.assign(14);
	vect3.resize(20,13);
	cout << "vect3 before copy: " << vect3 << '\n';

	vect3 = vect;
	cout << "vect3 after copy: " << vect3 << '\n';


	getchar();
	return 0;

}