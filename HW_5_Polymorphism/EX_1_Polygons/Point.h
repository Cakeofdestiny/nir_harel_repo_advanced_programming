#pragma once
#include <vector>
#include <cmath>
//#include "CImg.h"

using std::sqrt;
using std::pow;

class Point
{
public:
	Point(); //Add a default constructor
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);
	Point& operator=(const Point& other);

	double getX() const;
	double getY() const;

	double distance(const Point& other) const;

private:
	double _x;
	double _y;
	
};