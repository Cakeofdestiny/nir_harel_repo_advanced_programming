#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint 9000");
	_disp->display(*_board);
}

Menu::~Menu()
{
	this->_deleteAllShapes(); //make sure we free and delete all shapes
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::runMenu() // a manager function for the menu
{
	unsigned int userChoice = 0;
	do
	{
		this->_clearConsole(); //clear the console every iteration

		cout << "Enter 0 to add a new shape." << endl << 
				"Enter 1 to modify or get information about a current shape." << endl << 
				"Enter 2 to delete all of the shapes." << endl << 
				"Enter 3 to exit." << endl;

		cout << "Your choice: ";
		this->_getInputFromUser(userChoice);

		switch (userChoice)
		{
		case ADD_SHAPE:
			this->_addShape();
			break;

		case MODIFY_SHAPE:
			if (this->_shapes.size() > 0) //If there are no shapes to modify, we don't need to display this menu.
			{
				this->_modifyShape();
			}
			break;

		case DELETE_ALL:
			this->_deleteAllShapes();
			break;

		default: //No point to print an "INVALID CHOICE" error here, because the console will get cleared anyway.
			break;
		}
	} while (userChoice != EXIT);
}

void Menu::_addShape()
{
	unsigned int userChoice = 0;
	do
	{
		this->_clearConsole(); //clear the console every iteration

		cout << "Enter 0 to add a circle." << endl <<
				"Enter 1 to add an arrow." << endl <<
				"Enter 2 to add a triangle." << endl <<
				"Enter 3 to add a rectangle." << endl;

		cout << "Your choice: ";
		this->_getInputFromUser(userChoice); //safely get the uint input from the user

		switch (userChoice) //I wanted to use a fptr to a private method array but it was rather tricky
		{
		case OP_CIRCLE:
			this->_addCircle();
			break;

		case OP_ARROW:
			this->_addArrow();
			break;

		case OP_TRIANGLE:
			this->_addTriangle();
			break;

		case OP_RECTANGLE:
			this->_addRectangle();
			break;

		default:
			break;
		}
	} while (userChoice < 0 || userChoice >= NUM_SHAPES); //continue iterating while the shape isn't in range
}

void Menu::_modifyShape()
{
	unsigned int userChoice = 0;
	unsigned int shapeChoice = 0;
	int i = 0;
	do
	{
		this->_clearConsole();
		for  (i = 0; i < this->_shapes.size(); i++)//print the details of the shapes
		{
			cout << i << ". " << this->_shapes[i]->getName() << " | " << this->_shapes[i]->getType() << endl;
		}

		cout << "Please pick the shape you want to modify (with its index): ";
		this->_getInputFromUser(userChoice);


	} while (userChoice < 0 || userChoice >= this->_shapes.size()); //while it is invalid (out of the range of shapes in our array)

	shapeChoice = userChoice;

	do
	{
		this->_clearConsole(); //print the users choice 
		cout << "You picked: " << this->_shapes[shapeChoice]->getName() << " | " << this->_shapes[shapeChoice]->getType() << endl;

		cout << "Enter 0 to move the shape." << endl
			 << "Enter 1 to get its details." << endl
			 << "Enter 2 to remove the shape." << endl;
		 
		cout << "Your choice: ";
		this->_getInputFromUser(userChoice);

		switch (userChoice)
		{
		case OP_MOVE:
			this->_moveShape(shapeChoice);
			break;

		case OP_DETAILS:
			this->_shapes[shapeChoice]->printDetails();
			cout << endl; //because pause cmd will print on the same line as printDetails
			system(PAUSE_CMD); //pause to allow the user to read the shape's details before continuing
			break;
		case OP_REMOVE:
			this->_deleteShape(shapeChoice); //del the shape
			break;
		default:
			break;
		}
	} while (userChoice < 0 || userChoice > OP_REMOVE);

}

void Menu::_drawAllShapes() //Draw all shapes in our array
{
	for (Shape* const& shape : this->_shapes) //fancy cpp range
	{
		shape->draw(*(this->_disp), *(this->_board));
	}
}

void Menu::_addCircle()
{
	Point circleCenter;
	double radius;
	string circleName;
	Circle* circle;
	
	cout << "Please enter point values for the center of the circle: " << endl;
	circleCenter = this->_getPoint();

	cout << "Please enter the value for the radius of the circle: ";
	this->_getInputFromUser(radius);

	cout << "Please enter the name of the circle: ";
	cin >> circleName;

	circle = new Circle(circleCenter, radius, CIRCLE, circleName);
	circle->draw(*this->_disp, *this->_board); //We can draw it here since it'll go on top
	this->_shapes.push_back(circle);
}

void Menu::_addRectangle()
{
	Point rectCorner;
	double length = 0;
	double width = 0;
	string rectName;
	myShapes::Rectangle* rect = 0;

	cout << "Please enter point values for the top left corner of the rectangle: " << endl;
	rectCorner = this->_getPoint();

	//get length and width from the user
	
	cout << "Please enter the value for the length of the rectangle: ";
	this->_getInputFromUser(length);

	cout << "Please enter the value for the width of the rectangle: ";
	this->_getInputFromUser(width);

	cout << "Please enter the name of the rectangle: ";
	cin >> rectName;

	if (width <= 0 || length <= 0)
	{
		cout << WIDTH_LENGTH_ERR << endl;
		system(PAUSE_CMD);
	}
	else
	{
		rect = new myShapes::Rectangle(rectCorner, length, width, RECTANGLE, rectName); //create a new rect dynamically

		rect->draw(*this->_disp, *this->_board); //We can draw it here since it'll go on top

		this->_shapes.push_back(rect);
	}
}

void Menu::_addTriangle()
{
	Point triPoints[TRI_POINTS];
	string triName;
	Triangle* tri = 0;
	bool pointsInvalid = 0;

	cout << "Please enter values for the points of the triangle:" << endl << endl;
	for (int i = 0; i < TRI_POINTS; i++) //get the points from the user
	{
		cout << "Point number " << i+1 << ":" << endl;
		triPoints[i] = this->_getPoint();
	}

	cout << "Please enter the name of the triangle: ";
	cin >> triName;

	pointsInvalid = (triPoints[0].getX() == triPoints[1].getX() && triPoints[1].getX() == triPoints[2].getX()) || //IF all of the x values of the points are the same OR all of the y values of the points are the same
		(triPoints[0].getY() == triPoints[1].getY() && triPoints[1].getY() == triPoints[2].getY());				//the triangle is invalid since the points are on the same axis
		
	if (pointsInvalid)
	{
		cout << AXIS_ERR << endl;
		system(PAUSE_CMD);
	}
	else
	{
		tri = new Triangle(triPoints[0], triPoints[1], triPoints[2], TRIANGLE, triName); //create a new triangle
		tri->draw(*this->_disp, *this->_board); //draw it
		this->_shapes.push_back(tri); //and add it to our array
	}
}

void Menu::_addArrow()
{
	Point arrowPoints[ARROW_POINTS];
	string arrowName;
	Arrow* arrow = 0;

	cout << "Please enter values for the points of the arrow:" << endl << endl;
	for (int i = 0; i < ARROW_POINTS; i++) //get the points from the user
	{
		cout << "Point number " << i+1 << ":" << endl;
		arrowPoints[i] = this->_getPoint();
	}

	cout << "Please enter the name of the arrow: ";
	cin >> arrowName;
	
	arrow = new Arrow(arrowPoints[0], arrowPoints[1], ARROW, arrowName); //dynamically create a new arrow

	arrow->draw(*this->_disp, *this->_board); //draw it

	this->_shapes.push_back(arrow); //and add it to our array
}



void Menu::_moveShape(unsigned int id)
{
	Point movingScale;
	if (id >= this->_shapes.size()  || id < 0)
	{
		cout << ID_ERR << endl;
	}
	else
	{
		this->_clearConsole();
		cout << "Please enter the point describing the moving scale: " << endl;
		movingScale = this->_getPoint();

		this->_shapes[id]->clearDraw(*this->_disp, *this->_board); //clear the shape
		this->_shapes[id]->move(movingScale); //move it
		this->_drawAllShapes(); //draw all shapes including the moved shape	
	}
}

void Menu::_deleteShape(unsigned int id)
{
	if (id >= this->_shapes.size() || id < 0)
	{
		cout << ID_ERR << endl;
	}
	else
	{
		this->_shapes[id]->clearDraw(*this->_disp, *this->_board); //clear the element
		delete this->_shapes[id]; //free the element
		this->_shapes.erase(this->_shapes.begin() + id); //and erase the element from our shape vector
		this->_drawAllShapes(); //draw all shapes again, because it might've been on top of something

	}
}

inline void Menu::_clearConsole()
{
	system(CLEAR_CMD);
}

Point Menu::_getPoint()
{
	double pt_x = 0;
	double pt_y = 0;

	cout << "Enter the X value of the point: ";
	this->_getInputFromUser(pt_x);

	cout << "Enter the Y value of the point: ";
	this->_getInputFromUser(pt_y);
	
	return Point(pt_x, pt_y);
}

void Menu::_deleteAllShapes()
{
	//Iterate over the vector and free and pop all shapes after erasing them
	while (!this->_shapes.empty())
	{
		this->_shapes.back()->clearDraw(*(this->_disp), *(this->_board)); //actually erase the shape from the board
		delete this->_shapes.back();
		this->_shapes.pop_back();
	} 
}

template<class T> //a generic template function to allow us to use any type of input with a single declaration
inline void Menu::_getInputFromUser(T & val)
{
	bool cinFail = false;
	do
	{
		cin >> val;

		cinFail = cin.fail(); //store fail value

		cin.clear();
		cin.ignore(INT_MAX, '\n'); //Clear and flush cin incase the user entered a wrong values
	} while (cinFail);
}
