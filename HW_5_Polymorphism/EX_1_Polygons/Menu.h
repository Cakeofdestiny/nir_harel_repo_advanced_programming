#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

#if __linux__
#define CLEAR_CMD "clear"
#elif _WIN32
#define CLEAR_CMD "CLS"
#endif

#define PAUSE_CMD "PAUSE"

#define CIRCLE "Circle"
#define ARROW "Arrow"
#define RECTANGLE "Rectangle"
#define TRIANGLE "Triangle"

#define TRI_POINTS 3
#define ARROW_POINTS 2

#define WIDTH_LENGTH_ERR "Failed while creating a rectangle - either the length or width parameters were lower than or equal to 0."
#define AXIS_ERR "Failed while creating a triangle - all of your points were on the same axis."

#define ID_ERR "Internal Error | Invalid shape ID! You shouldn't see this error."
#define NUM_SHAPES 4

enum menuOption { ADD_SHAPE, MODIFY_SHAPE, DELETE_ALL, EXIT };
enum shapeOption { OP_CIRCLE, OP_ARROW, OP_TRIANGLE, OP_RECTANGLE};
enum modifyOption { OP_MOVE, OP_DETAILS, OP_REMOVE};

class Menu
{
public:

	Menu();
	~Menu();

	void runMenu(); //a manager function for the menu

	
private: 
	//ask the user for the shape they want to add, and direct them to the function.
	void _addShape();

	//User friendly functions to add all types of shapes
	void _addCircle();
	void _addRectangle();
	void _addTriangle();
	void _addArrow();


	//Ask the user which shape they want to modify, then ask them what type of modification they want to perform.
	void _modifyShape();

	void _moveShape(unsigned int id); //ask the user for a moving point, and move the shape.
	void _deleteShape(unsigned int id); //remove the shape 

	inline void _clearConsole();

	void _deleteAllShapes(); //Physically delete and free all shapes
	void _drawAllShapes(); //draw all shapes on the board

	template <class T>
	void _getInputFromUser(T& Val); //A generic function to get a validated value for any variable

	Point _getPoint(); //A function to get a point from the user

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	vector<Shape*> _shapes;
};

