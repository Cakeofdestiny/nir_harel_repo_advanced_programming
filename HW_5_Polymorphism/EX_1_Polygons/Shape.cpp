#include "Shape.h"

Shape::Shape(const string & name, const string & type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	cout << this->_type << "{name=\"" << this->_name << "\", area=\"" << this->getArea() << "\", perimeter=\"" << this->getPerimeter() << "\"}";
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}
