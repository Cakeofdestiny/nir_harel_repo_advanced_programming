#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
//#include "Cimg.h"

using namespace std;

class Polygon : public Shape
{
public:
	Polygon(const string& type, const string& name);
	virtual ~Polygon();

	virtual double getArea() const; //Unified functions for all polygons to compute area or perimeter
	virtual double getPerimeter() const;
	virtual void move(const Point& other);

protected:
	vector<Point> _points;
};