#include "Rectangle.h"

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[TOP_LEFT_IDX].getX(), _points[TOP_LEFT_IDX].getY(),
		_points[BOTTOM_RIGHT_IDX].getX(), _points[BOTTOM_RIGHT_IDX].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[TOP_LEFT_IDX].getX(), _points[TOP_LEFT_IDX].getY(),
		_points[BOTTOM_RIGHT_IDX].getX(), _points[BOTTOM_RIGHT_IDX].getY(), BLACK, 100.0f).display(disp);
}

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name) : Polygon(type, name)
{
	//Calculate the other points and push them back
	Point top_right(a.getX() + width, a.getY());
	Point bottom_left(a.getX(), a.getY() + length);
	Point bottom_right(a.getX() + width, a.getY() + length);

	//We chose this order because the area formula requires it to be counter clockwise
	this->_points.resize(4); //Number of vertices
	this->_points[TOP_LEFT_IDX] = a; //A is top left
	this->_points[1] = bottom_left;
	this->_points[BOTTOM_RIGHT_IDX] = bottom_right;
	this->_points[3] = top_right;
	
}

myShapes::Rectangle::~Rectangle() {}
