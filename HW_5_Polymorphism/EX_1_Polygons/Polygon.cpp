#include "Polygon.h"

Polygon::Polygon(const string & type, const string & name) : Shape(name, type)
{
}

Polygon::~Polygon() {}


//A general function to calculate the area of any polygon
//We're using this formula: |((points[0..n-1].x * points[1..n].y) - (points[0..n-1].y * points[1..n].x)) / 2|
//Essentially, substract the sum of products of xs and following ys, by the sum of products of ys and following xs, and divide by 2. The result is the absolute value.
double Polygon::getArea() const
{
	double xy_sum = 0.0; //product of x[i] and y[i+1]
	double yx_sum = 0.0; //product of y[i] and x[i+1]
	unsigned int num_points = this->_points.size();
	unsigned int next_loc = 0;

	for (unsigned int i = 0; i < num_points; i++)
	{
		next_loc = (i + 1) % num_points;
		xy_sum += this->_points[i].getX() * this->_points[next_loc].getY(); //points[i].x * points[i+1].y
		yx_sum += this->_points[i].getY() * this->_points[next_loc].getX(); //points[i].y * points[i+1].x
	}
	return abs((xy_sum - yx_sum) / 2); //Using this formula, we can calculate the area fo any polygon.
}

double Polygon::getPerimeter() const
{
	double perimeter = 0.0;

	//Loop until len of points -1
	//Each time, add the distance of the current point to the next point, and finally add the distance from the last point to the first point.
	//For example, for a triangle: perimeter += points[0].distance(points[1])
	//							   perimeter += points[1].distance(points[2])
	//								
	for (int i = 0; i < this->_points.size() - 1; i++)
	{
		perimeter += this->_points[i].distance(this->_points[i + 1]);
	}
	//After the loop:              perimeter += points[2].distance(points[0])
	perimeter += this->_points[this->_points.size() - 1].distance(this->_points[0]);

	return perimeter;
}

void Polygon::move(const Point & other)
{
	for (int i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}
