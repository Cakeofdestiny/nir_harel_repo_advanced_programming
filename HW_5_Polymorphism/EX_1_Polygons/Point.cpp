#include "Point.h"

Point::Point() : _x(0), _y(0) {} 

Point::Point(double x, double y) : _x(x), _y(y)  {} //Regular constructor for point

Point::Point(const Point & other)
{
	*this = other; //use assignment op to copy
}

Point::~Point() {}

Point Point::operator+(const Point & other) const
{
	int new_x = this->_x + other._x;
	int new_y = this->_y + other._y;

	return Point(new_x, new_y); //Create a new object with the desired properties
}

Point & Point::operator+=(const Point & other)
{
	this->_x += other._x;
	this->_y += other._y;

	return *this; 
}

Point & Point::operator=(const Point & other)
{
	this->_x = other._x;
	this->_y = other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point & other) const
{
	int x_diff = this->_x - other._x;
	int y_diff = this->_y - other._y;
	//Use pythagorean theorem to calculate the distance
	return sqrt(pow(x_diff, 2) + pow(y_diff, 2));
}
