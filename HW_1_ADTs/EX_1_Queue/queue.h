#ifndef QUEUE_H
#define QUEUE_H

#define EMPTY_QUEUE -1

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* queue_arr;
	int max_size;
	unsigned int last_idx;
	unsigned int first_idx;
	int current_size;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */