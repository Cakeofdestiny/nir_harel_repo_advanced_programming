#include "queue.h"

/* a queue contains positive integer values. */

//

///Initializes a queue
///q is an empty queue struct.
void initQueue(queue * q, unsigned int size)
{
	q->queue_arr = new unsigned int[size];
	q->max_size = size;
	q->first_idx = 0;
	q->last_idx = 0;
	q->current_size = 0;
}

///Clean the queue.
///q is an initialized queue struct.
void cleanQueue(queue* q)
{
	while (dequeue(q) != -1) {}
}

///Enqueue an unsigned int.
void enqueue(queue * q, unsigned int newValue)
{
	//If the queue is not full
	if (q->current_size < q->max_size)
	{
		q->queue_arr[q->last_idx] = newValue;
		q->last_idx = (q->last_idx + 1) % q->max_size;
		q->current_size += 1;
	}
}

///Returns the element at the top of the queue, or -1 if the queue is empty.
int dequeue(queue * q)
{	
	int resp = 0;

	if (q->current_size == 0)
	{
		resp = EMPTY_QUEUE;
	}else
	{
		resp = q->queue_arr[q->first_idx];
		q->queue_arr[q->first_idx] = 0;
		//This is to allow the queue to wrap over. Otherwise, we would only be able to perform 'size' operations.
		q->first_idx = (q->first_idx + 1) % q->max_size;
		q->current_size -= 1;
	}
	return resp;
}
