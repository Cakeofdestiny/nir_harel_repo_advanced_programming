#include "utils.h"
#include <iostream>


//I: array of nums, length of the array
void reverse(int * nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;
	initStack(s);

	//Push numbers into the stack
	for (i = 0; i<size; i++)
	{
		push(s, nums[i]);
	}

	//Now, go over the array again. The stack's LIFO nature will reverse it.
	for (i=0; i<size; i++)
	{
		nums[i] = pop(s);
	}

	cleanStack(s);
	delete s;
}


//The function will ask the user to enter 10 whole numbers, and return them in an opposite order.
int * reverse10()
{
	int* arr = new int[ARR_LENGTH];
	int i = 0;

	for (i=0; i<ARR_LENGTH; i++)
	{
		std::cout << "Please enter a number: ";
		std::cin >> arr[i];
	}
	
	reverse(arr, ARR_LENGTH);

	return arr;
}
