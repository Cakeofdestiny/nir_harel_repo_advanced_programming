#pragma once

// Link (node) struct
typedef struct Node
{
	unsigned int num;
	struct Node* next;
} Node;

void insertAtStart(Node** head, Node* newNode);
void DeleteFromStart(Node** head);

int listLength(Node* first);
void freeList(Node** head); 


