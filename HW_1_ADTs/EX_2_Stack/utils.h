#ifndef UTILS_H
#define UTILS_H

#include "stack.h"

constexpr auto ARR_LENGTH = 10;

void reverse(int* nums, unsigned int size);
int* reverse10();

#endif // UTILS_H