#include "linkedList.h"


//I: pointer to a pointer to the head node, node to add
//adds a node at the beginning
void insertAtStart(Node** head, Node* newNode)
{
	Node* curr = *head;
	//Set the head as the new node
	*head = newNode;
	//set the second node to be the previous head
	newNode->next = curr;
}

//I: pointer to a pointer to the head node
//deletes a node from the beginning
void DeleteFromStart(Node** head)
{
	Node* curr = *head;
	//We don't want to accidentally free location 0
	if (curr)
	{
		//Set the origin pointer to the next node. 
		*head = curr->next;
		delete curr;
	}
}


//I: The first element in a linked list.
//O: The number of nodes until the end of the list
int listLength(Node* first)
{
	int amountOfElements = 0;
	if (first)
	{
		amountOfElements = listLength(first->next) + 1;
	}
	return amountOfElements;
}

//I: pointer to a pointer to the head
//Frees the linked list.
void freeList(Node** head)
{
	Node* curr = *head;
	Node* next = nullptr;
	while (curr)
	{
		//save the next node
		next = curr->next;
		//delete the current one
		delete curr;
		//exchange
		curr = next;
	}
}



