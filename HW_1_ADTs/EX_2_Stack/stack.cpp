#include "stack.h"


//Initialize the stack
void initStack(stack * s)
{
	s->_head = 0;
}

//I: Stack struct pointer, new int
void push(stack* s, unsigned int element)
{
	Node* newnode = new Node;
	newnode->num = element;
	insertAtStart(&s->_head, newnode);
}


//I: stack struct pointer
//o: popped value, -1 if there are no values
int pop(stack* s)
{
	int resp = 0;
	if (s->_head)
	{
		resp = s->_head->num;
		DeleteFromStart(&s->_head);
	}else
	{
		resp = STACK_EMPTY;
	}

	return resp;
}

//I: Stack struct pointer
//cleans the stack
void cleanStack(stack* s)
{
	while (s->_head)
	{
		DeleteFromStart(&s->_head);
	}
}

