#pragma comment(lib, "../Debug/stream.lib")
#include "../stream/FileStream.h"
#include "../stream/OutStream.h"
#define OK 0;

int main()
{
	//Use our library
	msl::OutStream ostream;
	ostream << "I'm the Doctor and I'm " << 1500 << " years old.";

	msl::FileStream fstream;
	fstream.open("test.txt", "w");

	fstream << "writing to file... " << 34 << ", newline now" << msl::endline;

	fstream.close();

	getchar();

	return OK;
}