#include "FileStream.h"

namespace msl
{
	FileStream::FileStream() : OutStream() {}

	//Close the file
	FileStream::~FileStream()
	{
		this->close(); //We basically only need to make sure that the file is closed.
	}

	//I: filename, opening type
	//Opens the file and sets it to stream.
	int FileStream::open(const char * filename, const char * open_type)
	{
		FILE* file = fopen(filename, open_type);
		if (file)
		{
			this->stream = file;
		}
		else
		{
			this->stream = 0;
		}
		return this->stream != 0; //If the stream is not equal to 0, it'll return 0 which means it did not fail.
	}

	void FileStream::close()
	{
		//Check if the stream is open, and close it if it is.
		if (this->stream)
		{
			fclose(this->stream);
			this->stream = 0;
		}
	}
}
