#include "OutStreamEncrypted.h"
#include <math.h>
#include <stdlib.h>

//Constructor
OutStreamEncrypted::OutStreamEncrypted(unsigned int offset) : OutStream()
{
	this->_offset = offset;
}

OutStreamEncrypted::~OutStreamEncrypted() {}

OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
	int len = strlen(str);
	char* encrypted_string = new char[len + 1]; //dynamically allocate a new string + 1 for null terminator
	this->encrypt_string(str, encrypted_string); //encrypt it
	OutStream::operator<<(encrypted_string); //use our parent classes' operator instead of implementing our own

	delete[] encrypted_string;

	return *this;
}

//For both of these, just use the same functions as the parent class, because they require no modification.

OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	char* encrypted_string = new char[ceil(log10(num)) + 1];  //Get the number of digits with ceil log10, and add 1 for the null terminator.

	sprintf(encrypted_string, "%d",num);
	encrypt_string(encrypted_string, encrypted_string); //we don't need to preserve this string, so we can just modify the same.

	OutStream::operator<<(encrypted_string);

	delete[] encrypted_string; //free memory
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *))
{
	OutStream::operator<<(pf);
	return *this;
}

//I: source and dst strings. 
//Encrypts according to the class attribute _offset
void OutStreamEncrypted::encrypt_string(const char * src, char * dst) const
{
	int src_len = strlen(src);
	for (int i = 0; i < src_len; i++)
	{
		//First normalize the ascii char down to 0, add the offset to it, make sure it's in range with module and add the beginning of the range back to it.
		dst[i] = (src[i] - ASCII_START + this->_offset) % (ASCII_RANGE_LEN)+ASCII_START;
	}
	dst[src_len] = 0; //Make sure we have the null terminator.
}
