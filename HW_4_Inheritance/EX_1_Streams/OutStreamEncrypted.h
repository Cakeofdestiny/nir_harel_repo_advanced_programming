#pragma once
#include "OutStream.h"
#include "string.h"

#define ASCII_START 32
#define ASCII_END 127 //We need to make it 127, because the range is inclusive on both sides.
#define ASCII_RANGE_LEN ASCII_END - ASCII_START
#define BASE_10 10

class OutStreamEncrypted : public OutStream
{
public:
	OutStreamEncrypted(unsigned int offset);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE*)); //Since overriding one operator<< will override them all, we have to copy them.

private:
	void encrypt_string(const char * src, char * dst) const;

private:
	unsigned int _offset;

};
