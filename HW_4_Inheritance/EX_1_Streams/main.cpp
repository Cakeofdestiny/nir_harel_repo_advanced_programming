#include "OutStream.h"
#include "OutStreamEncrypted.h"
#include "FileStream.h"
#include "Logger.h"

int main(int argc, char **argv)
{
	OutStream stream;

	stream << "I am the Doctor and I'm " << 1500 << " years old." << endline;

	FileStream fstream;
	fstream.open("test.txt", "w");

	fstream << "I am the Doctor and I'm " << 1500 << " years old." << endline;

	//fstream.close();
	// we don't even need to close it as it'll be destructed automatically


	OutStreamEncrypted estream(1); //create an encrypted stream with an offset of 1
	estream << "I am the Doctor and I'm " << 1500 << " years old." << endline; //Check with the doctor string
	
	Logger logger1;
	logger1 << "Logging now..." << "newline after this" << endline << "now in a new line." << endline;

	logger1 << "should be the third line" << endline;

	Logger logger2;

	logger2 << "on a new instance, should be the fourth line...";

	getchar();
	return 0;
}
