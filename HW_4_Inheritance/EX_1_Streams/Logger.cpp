#include "Logger.h"

Logger & operator<<(Logger & l, const char * msg)
{
	l.setStartLine(); //use the set start line to print in case it's the beginning of the line
	l.os << msg;
	return l; // return l to allow for chaining
}

Logger & operator<<(Logger & l, int num)
{
	l.setStartLine(); //use the set start line to print in case it's the beginning of the line
	l.os << num;
	return l; //return the logger to allow for chaining
}

Logger & operator<<(Logger & l, void(*pf)(FILE* stream))
{
	l.setStartLine(); //use the set start line to print in case it's the beginning of the line
	l.os << pf; //
	l._startLine = true; //Because we finished the line with endline, startLine is now true
	return l; //return the logger to allow for chaining
}

void Logger::setStartLine()
{
	static unsigned int curr_line = 1; //static var to count lines
	if (this->_startLine) //if this is the beginning of the line
	{
		this->_startLine = false; //set it to false to make sure it doesn't print the prefix again
		*this << LOG_PREFIX << curr_line << END_LOG_PREFIX; //Print the log prefix: "LOG {LINE_NUM}:"
		curr_line++; //increment the line
	}
}

Logger::Logger()
{
	this->_startLine = true; //because the first line will be a new line
}

Logger::~Logger()
{
}
