#include "OutStream.h"


OutStream::OutStream()
{
	this->stream = stdout;
}

OutStream::~OutStream() {}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->stream, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->stream, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE*))
{
	pf(this->stream);
	return *this;
}


void endline(FILE* stream)
{
	fprintf(stream, "\n");
}
