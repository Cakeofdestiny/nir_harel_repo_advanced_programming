#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	//Ctor and dtor
	FileStream();
	~FileStream();

	int open(const char* filename, const char * open_type); //open file with filename and opening type
	void close();
};