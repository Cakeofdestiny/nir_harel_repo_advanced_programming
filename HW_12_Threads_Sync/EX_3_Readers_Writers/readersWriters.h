#pragma once
#include <mutex>
#include <condition_variable>
#include <string>
#include <fstream>
#include <iostream>

#define ERR_INVALID_LINE_READ "The requested line doesn't exist."
#define ERR_INVALID_LINE_WRITE "The line you're trying to write to is invalid."

#define ERR_INVALID_FILE_READ "I could not open the file for reading."
#define ERR_INVALID_FILE_WRITE "I could not open the file for writing."

#define NEWLINE '\n'

class readersWriters
{
private:
	std::mutex _mu;
	//There is no need for this class member as unique locks are supposed to be created locally, and uniquely for each thread
	//std::unique_lock<std::mutex> _locker;
	std::condition_variable _condW;
	std::condition_variable _condR;
	int _readersNumber;
	int _writersNumber;
	std::string _fileName;
	void _advanceLines(std::fstream& filestream, int n);

public:
	readersWriters(std::string fileName);

	//The following two classes are BasicLockable classes. This means that we can use them as templates for a lock guard or unique lock.
	//Details: https://en.cppreference.com/w/cpp/named_req/BasicLockable
	class readerLock
	{
	public:
		readerLock(readersWriters& parent);
		void lock();
		void unlock() noexcept;

	private:
		readersWriters& _parent;
	};

	class writerLock
	{
	public:
		writerLock(readersWriters& parent);
		void lock();
		void unlock() noexcept;

	private:
		readersWriters& _parent;
	};
	
	std::string readLine(int lineNumber); //lineNumber - line number to read
	void WriteLine(int lineNumber, std::string newLine);//lineNumber - line number to write 
};


