#include "readersWriters.h"

#define OK 0

int main()
{
	//These checks do not check if the actual concurrency protection works, but only if the write and read funcs work

	//This assumes that the file reads: yesa\nyesb\n\nyesc
	readersWriters rwObject("testfile.txt");
	//read tests
	std::cout << rwObject.readLine(0) << std::endl; //the following 3 should be normal
	std::cout << rwObject.readLine(1) << std::endl;
	std::cout << rwObject.readLine(2) << std::endl; //this should be an empty string
	std::cout << rwObject.readLine(3) << std::endl;
	std::cout << rwObject.readLine(4) << std::endl; //this should print that it doesn't exist

	rwObject.WriteLine(4, "yesd");
	std::cout << rwObject.readLine(4) << std::endl; //should now print yesd
	rwObject.WriteLine(0, "no"); 
	std::cout << rwObject.readLine(0) << std::endl; //should now print nosa
	rwObject.WriteLine(6, "anything"); //should print error because it's over 1 line beyond the end

	system("pause");
	return OK;
}