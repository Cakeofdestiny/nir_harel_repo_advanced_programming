#include "readersWriters.h"

//This function advances the cursor of the file n lines
//It does not guarantee safety from EOF
void readersWriters::_advanceLines(std::fstream & fileStream, int n)
{
	for (int i = 0; i < n; i++) 
	{
		fileStream.ignore(std::numeric_limits<std::streamsize>::max(), NEWLINE);
	}
	//also seek the writing position to this location
	fileStream.seekp(fileStream.tellg()); 
}

readersWriters::readersWriters(std::string fileName)
{
	//std::unique_lock<std::mutex> tmp_lock(this->_mu, std::defer_lock);
	//this->_locker.swap(tmp_lock);
	this->_fileName = fileName;
	this->_readersNumber = 0;
	this->_writersNumber = 0;
}

std::string readersWriters::readLine(int lineNumber)
{
	std::string line = "";
	readerLock readLock(*this);
	//We can use this because I implemented a BasicLockable type class
	std::lock_guard<readerLock> lock(readLock); 
	//I'm using fstream and not ifstream for compatibility with advancelines
	std::fstream file(this->_fileName, std::ios::in);
	if (file.is_open())
	{
		this->_advanceLines(file, lineNumber);
		if (file.eof())
		{
			std::cerr << ERR_INVALID_LINE_READ << std::endl;
		}
		else
		{
			getline(file, line);
		}
	}
	else
	{
		std::cerr << ERR_INVALID_FILE_READ << std::endl;
	}
	
	return line;
}

void readersWriters::WriteLine(int lineNumber, std::string newLine)
{
	std::string line = "";
	writerLock writeLock(*this);
	//We can use this because I implemented a BasicLockable type class
	std::lock_guard<writerLock> lock(writeLock);
	//I'm using append to open the file without overwriting it, but we need to seek back manually
	std::fstream file(this->_fileName);

	if (file.is_open())
	{
		if (lineNumber == 0) //if the new line number is 0 we can write a new line safely
		{
			file << newLine;
		}
		else
		{
			this->_advanceLines(file, lineNumber-1);
			//If it's already EOF, it means that the line before our line doesn't exist -- we can't write.
			if (file.eof()) 
			{
				std::cerr << ERR_INVALID_LINE_WRITE << std::endl;
			}
			else
			{
				//go to the next line to write
				this->_advanceLines(file, 1);
				//This means that we just need to add to the end of the file, so I'll just reopen it in append mode
				if (file.eof()) 
				{				
					file.close();
					file.open(this->_fileName, std::fstream::app);
					//Add a newline because we're moving to a new line
					file << std::endl;
				}
				file << newLine;
			}
		}
		file.close();
	}
	else
	{
		std::cerr << ERR_INVALID_FILE_WRITE << std::endl;
	}
}

readersWriters::readerLock::readerLock(readersWriters& parent): _parent(parent) {}

void readersWriters::readerLock::lock()
{
	//we can't use the class's unique lock because ul is not thread safe..
	std::unique_lock<std::mutex> lock(this->_parent._mu);
	//lambda for spurious wakeup
	this->_parent._condR.wait(lock, [this] {return this->_parent._writersNumber == 0; });
	this->_parent._readersNumber++;
}

void readersWriters::readerLock::unlock() noexcept
{
	this->_parent._readersNumber--;
	//If there aren't any other readers, we can notify a writer
	if (this->_parent._readersNumber == 0)
	{
		this->_parent._condW.notify_one();
	}
}

readersWriters::writerLock::writerLock(readersWriters & parent): _parent(parent) {}

void readersWriters::writerLock::lock()
{
	//Even though this lock WILL release at the end of the scope, it does not matter as we're using predicates
	std::unique_lock<std::mutex> lock(this->_parent._mu);
	//If anyone is reading or writing, we need to wait until they finish
	//the lambda makes sure of this
	this->_parent._condW.wait(lock, [this] {return this->_parent._readersNumber == 0 && this->_parent._writersNumber == 0; });
	this->_parent._writersNumber++;
}

void readersWriters::writerLock::unlock() noexcept
{
	this->_parent._writersNumber--;
	//I'm not using _locker, the class member. It's wrong.
	//this->_locker.unlock();
	this->_parent._condR.notify_all();
	/*Even though it may seem like then predicate will fail and the writer will not activate
	/if there are any readers still reading, the writer will still be notified correctly
	/as readUnlock() calls the condition for the writer anyway.*/
	this->_parent._condW.notify_one();
}
