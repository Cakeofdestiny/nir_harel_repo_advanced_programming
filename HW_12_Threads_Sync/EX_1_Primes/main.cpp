#include "threads.h"

int main()
{	
	unsigned int threads = std::thread::hardware_concurrency();
	threads = threads ? threads : 1; //Run on at least 1 hardware thread if the function returned 0.

	cout << "Running multithreaded functions using " << threads << " threads, based on HW support" << endl;

	callWritePrimesMultipleThreads(0, 1000, "primes1.txt", threads);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", threads);
	callWritePrimesMultipleThreads(0, 1000000, "primes3.txt", threads);

	system("pause");
	return 0;
}