#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

using namespace std::chrono; //a relatively small namespace so it's no harm
using std::thread;
using std::ofstream;
using std::string;
using std::cout;
using std::endl;

void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
