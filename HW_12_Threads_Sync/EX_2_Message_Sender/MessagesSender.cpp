﻿#include "MessagesSender.h"

void clearConsole()
{
	system(CLEAR_CMD);
}

MessagesSender::MessagesSender() 
{
	std::thread(&MessagesSender::_readMessagesIntoQueue, this).detach(); //no need to even preserve the thread, as we immediately detach it
	std::thread(&MessagesSender::_sendMessagesToOutput, this).detach();
	this->_continueReading = true;
	this->_continueSending = true;
}

MessagesSender::~MessagesSender() 
{
	this->_continueReading = false;
	this->_continueSending = false;
}

void MessagesSender::runMenu()
{
	unsigned int userChoice; //Unsigned int and not enum because it's simpler with cin that way
	do
	{
		clearConsole();
		this->_printMenu();
		cout << endl << "Your choice: ";
		getInputFromUser(userChoice);

		switch (userChoice)
		{
		case SIGN_IN:
			this->_signIn();
			break;

		case SIGN_OUT:
			this->_signOut();
			break;

		case CONNECTED_USERS:
			this->_connectedUsers();
			break;

		default:
			break;
		}
	} while (userChoice != EXIT);
}

void MessagesSender::_signIn()
{
	string name = "";
	clearConsole();
	
	cout << "MessageSystem signin || Please enter your username: ";
	getInputFromUser(name);

	this->_userSetMutex.lock();
	//if the second item is true it means that we inserted the element
	if (this->_userSet.insert(name).second) 
	{
		std::cout << "Successfully signed in." << endl;
	}
	else
	{
		std::cout << "The user was already signed into the system." << endl;
	}
	this->_userSetMutex.unlock();
	system(PAUSE_CMD);
}

void MessagesSender::_signOut()
{
	string name = "";
	clearConsole();
	
	cout << "MessageSystem sign out || Please enter your username: ";
	getInputFromUser(name);

	this->_userSetMutex.lock();
	if (this->_userSet.erase(name) > 0) //erase returns the number of deletions
	{
		std::cout << "Successfully signed out." << endl;
	}
	else
	{
		std::cout << "The user wasn't logged into the system." << endl;
	}
	this->_userSetMutex.unlock();
	system(PAUSE_CMD);
}

void MessagesSender::_connectedUsers()
{
	cout << "Printing logged in users: " << endl;
	for (std::set<string>::iterator it = this->_userSet.begin(); it != this->_userSet.end(); ++it)
	{
		cout << "* " << *it << endl; 
	}
	system(PAUSE_CMD);
}

void MessagesSender::_printMenu()
{
	cout << SIGN_IN << ". Sign in" << endl
		<< SIGN_OUT << ". Sign out" << endl
		<< CONNECTED_USERS << ". View connected users" << endl
		<< EXIT << ". Exit" << endl;
}

void MessagesSender::_readMessagesIntoQueue()
{
	std::fstream dataFile;
	string lineString;
	while (this->_continueReading) //loop forever
	{
		dataFile.open(DATA_FILE);

		if (dataFile.is_open()) //make sure we can open it
		{
			while (getline(dataFile, lineString))
			{
				std::lock_guard<std::mutex> lock(this->_queueMutex); //when using files, we might have exceptions
				this->_msgQueue.push(lineString);
			}
			
			this->_newMessageCV.notify_one(); //notify the reading thread

			dataFile.close();
			dataFile.open(DATA_FILE, std::ios::out | std::ios::trunc); //truncating will delete the file's contents
			dataFile.close();
		}
		
		std::this_thread::sleep_for(DATA_READ_SLEEP);
	}
}

void MessagesSender::_sendMessagesToOutput()
{
	std::unique_lock<std::mutex> lock(this->_queueMutex);
	std::ofstream outputFile;
	set<string>::iterator it;
	string msg;
	while (this->_continueSending)
	{
		//Just wait until we get notified, and then check if the queue isn't empty with the lambda func
		this->_newMessageCV.wait(lock, [this] {return !this->_msgQueue.empty();}); 
		while (!this->_msgQueue.empty())
		{
			msg = this->_msgQueue.front();
			outputFile.open(OUTPUT_FILE, std::ios_base::app);
			if (outputFile.is_open())
			{
				//Iterate over all of the users in our set, and "send" the message.
				for (it = this->_userSet.begin(); it != this->_userSet.end(); ++it)
				{
					outputFile << *it << ": " << msg << endl;
				}
				outputFile.close();
			}
			this->_msgQueue.pop(); // delete the message from the queue	
		}
	}
}

