#pragma once

#include <iostream>
#include <mutex>
#include <condition_variable>
#include <string>
#include <set>
#include <chrono>
#include <queue>
#include <fstream>

#define CLEAR_CMD "CLS"
#define PAUSE_CMD "PAUSE"
#define DATA_FILE "data.txt"
#define OUTPUT_FILE "output.txt"
#define DATA_READ_SLEEP 60s

typedef enum MenuChoice {SIGN_IN = 1, SIGN_OUT, CONNECTED_USERS, EXIT} MenuChoice;

using namespace std::chrono_literals;
using std::queue;
using std::set;
using std::string;
using std::cout;
using std::endl;
using std::cin;

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void runMenu();
private:
	void _signIn();
	void _signOut();
	void _connectedUsers();
	void _printMenu();

	//Threaded functions
	void _readMessagesIntoQueue(); 
	void _sendMessagesToOutput(); 

	set<string> _userSet;
	queue<string> _msgQueue;

	std::mutex _userSetMutex;
	std::mutex _queueMutex; 
	std::condition_variable _newMessageCV;
	bool _continueReading;
	bool _continueSending;
};

void clearConsole();

template<class T> //a generic template function to allow us to use any type of input with a single declaration
void getInputFromUser(T & val)
{
	bool cinFail = false;
	do
	{
		cin >> val;
		cinFail = cin.fail(); //store fail value

		cin.clear();
		cin.ignore(INT_MAX, '\n'); //Clear and flush cin incase the user entered a wrong values
	} while (cinFail);
};
